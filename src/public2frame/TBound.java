/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

import java.util.ArrayList;

/**
 *
 * @author apple
 */
public class TBound {

    public final Float xmin, xmax, ymin, ymax, zmin, zmax;

    public TBound(Float xmin, Float xmax, Float ymin, Float ymax, Float zmin, Float zmax) {
        this.xmin = xmin;
        this.xmax = xmax;
        this.ymin = ymin;
        this.ymax = ymax;
        this.zmin = zmin;
        this.zmax = zmax;
    }
    
    public TPoint center() {
        return new TPoint((xmax + xmin)/2, (ymax + ymin)/2, (zmax + zmin)/2);
    }

    TBound(ArrayList<TPoint> points) {
        Float tempXmin = Float.MAX_VALUE;
        Float tempYmin = Float.MAX_VALUE;
        Float tempZmin = Float.MAX_VALUE;

        Float tempXmax = -Float.MAX_VALUE;
        Float tempYmax = -Float.MAX_VALUE;
        Float tempZmax = -Float.MAX_VALUE;

        for (TPoint p : points) {
            tempXmax = Math.max(tempXmax, p.x);
            tempYmax = Math.max(tempYmax, p.y);
            tempZmax = Math.max(tempZmax, p.z);

            tempXmin = Math.min(tempXmin, p.x);
            tempYmin = Math.min(tempYmin, p.y);
            tempZmin = Math.min(tempZmin, p.z);
        }

        xmin = tempXmin;
        xmax = tempXmax;

        ymin = tempYmin;
        ymax = tempYmax;

        zmin = tempZmin;
        zmax = tempZmax;
    }

    Float dx() {
        return Math.abs(xmax - xmin);
    }

    Float dy() {
        return Math.abs(ymax - ymin);
    }

    Float dz() {
        return Math.abs(zmax - zmin);
    }

    public boolean isContainVoxel(TVoxel voxel) {
        boolean isInvalidXmin = xmin >= voxel.xmax;
        boolean isInvalidXmax = xmax <= voxel.xmin;
        boolean isInvalidYmin = ymin >= voxel.ymax;
        boolean isInvalidYmax = ymax <= voxel.ymin;
        boolean isInvalidZmin = zmin >= voxel.zmax;
        boolean isInvalidZmax = zmax <= voxel.zmin;

        boolean isInvalid = isInvalidXmin || isInvalidXmax || isInvalidYmin || isInvalidYmax || isInvalidZmin || isInvalidZmax;
        return !isInvalid;
    }
    
    TPoint centerPoint() {
        return new TPoint(
                (xmax - xmin) / 2, 
                (ymax - ymin) / 2, 
                (zmax - zmin) / 2
        );
    }

    @Override
    public String toString() {
        Float size = Math.abs(xmax - xmin) * Math.abs(ymax - ymin) * Math.abs(zmax - zmin);
        return String.format("TBound MIN(%f, %f, %f) MAX (%f, %f, %f), size= %f", xmin, ymin, zmin, xmax, ymax, zmax, size);
    }

}
