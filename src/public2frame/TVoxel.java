/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

import java.util.ArrayList;
import org.locationtech.jts.geom.Polygon;

/**
 *
 * @author apple
 */
public class TVoxel {

    public final float xmin, ymin, zmin, xmax, ymax, zmax;
    public int xi = -1, yi = -1, zi = -1;
    public float mean = 0;

    /**
     *
     */
    public TColor color = new TColor((float) 0.0, (float) 1.0, (float) 0.156862745098039);

    public TVoxel(float xmin, float ymin, float zmin, float xmax, float ymax, float zmax) {
        this.xmin = xmin;
        this.ymin = ymin;
        this.zmin = zmin;
        this.xmax = xmax;
        this.ymax = ymax;
        this.zmax = zmax;
    }

    public Float dx() {
        return Math.abs(xmax - xmin);
    }

    public Float dy() {
        return Math.abs(ymax - ymin);
    }

    public Float dz() {
        return Math.abs(zmax - zmin);
    }

    public void setIndexOfXYZ(int xi, int yi, int zi) {
        this.xi = xi;
        this.yi = yi;
        this.zi = zi;
    }

    public TVoxel(ArrayList<TPoint> points, String tranlationLine) {
        String[] tranlationParts = tranlationLine.replace("translation", "").trim().split(" ");

        float temptXmin = Float.MAX_VALUE;
        float temptYmin = Float.MAX_VALUE;
        float temptZmin = Float.MAX_VALUE;
        float temptXmax = -Float.MAX_VALUE;
        float temptYmax = -Float.MAX_VALUE;
        float temptZmax = -Float.MAX_VALUE;

        for (TPoint point : points) {
            temptXmax = Math.max(temptXmax, point.x);
            temptXmin = Math.min(temptXmin, point.x);

            temptYmax = Math.max(temptYmax, point.y);
            temptYmin = Math.min(temptYmin, point.y);

            temptZmax = Math.max(temptZmax, point.z);
            temptZmin = Math.min(temptZmin, point.z);
        }

        this.xmin = temptXmin + Float.parseFloat(tranlationParts[0]);
        this.ymin = temptYmin + Float.parseFloat(tranlationParts[1]);
        this.zmin = temptZmin + Float.parseFloat(tranlationParts[2]);
        this.xmax = temptXmax + Float.parseFloat(tranlationParts[0]);
        this.ymax = temptYmax + Float.parseFloat(tranlationParts[1]);
        this.zmax = temptZmax + Float.parseFloat(tranlationParts[2]);
    }
    
    public boolean isNearTo(TVoxel checkedVx) {
        float khoangCach2Tam = centerPoint().khoangCach(checkedVx.centerPoint());
        return duongCheo() >= khoangCach2Tam;
    }

    public float duongCheo() {
        TPoint t1 = new TPoint(xmin, ymin, 0);
        TPoint t2 = new TPoint(xmax, ymax, 0);
        return t1.khoangCach(t2);
    }
    
    public TVoxel nextVoxel(int xi, int yi, int zi, Float dx, Float dy, Float dz) {
        return new TVoxel(
                xmin + dx * xi,
                ymin + dy * yi,
                zmin + dz * zi,
                xmax + dx * xi,
                ymax + dy * yi,
                zmax + dz * zi
        );
    }

    public boolean isContainPoint(Float x, Float y, Float z) {
        Float doChenhLechLamTronSo = (float) 0.00001;
        return (xmin - doChenhLechLamTronSo <= x && xmax + doChenhLechLamTronSo >= x)
                && (ymin - doChenhLechLamTronSo <= y && ymax + doChenhLechLamTronSo >= y)
                && (zmin - doChenhLechLamTronSo <= z && zmax + doChenhLechLamTronSo >= z);
    }

    public Float size() {
        return Math.abs(xmax - xmin) * Math.abs(ymax - ymin) * Math.abs(zmax - zmin);
    }

    public TPoint centerPoint() {
        float x = xmin + (xmax - xmin) / 2;
        float y = ymin + (ymax - ymin) / 2;
        float z = zmin + (zmax - zmin) / 2;

        return new TPoint(x, y, z);
    }

    public ArrayList<TPoint> all2DVertexs() {
        ArrayList<TPoint> output = new ArrayList<>();
        output.add(new TPoint(xmin, ymin, 0));
        output.add(new TPoint(xmax, ymin, 0));
        output.add(new TPoint(xmax, ymax, 0));
        output.add(new TPoint(xmin, ymax, 0));
        return output;
    }

    public ArrayList<TPoint> all2DVertexsForPolygon() {
        ArrayList<TPoint> output = new ArrayList<>();
        output.add(new TPoint(xmin, ymin, 0));
        output.add(new TPoint(xmax, ymin, 0));
        output.add(new TPoint(xmax, ymax, 0));
        output.add(new TPoint(xmin, ymax, 0));
        output.add(new TPoint(xmin, ymin, 0));
        return output;
    }

    public Polygon polygon() {
        return Util.polygonFrom(this.all2DVertexsForPolygon());
    }

    private Polygon _cachedPolygon;

    public Polygon cachedPolygon() {
        if (_cachedPolygon == null) {
            _cachedPolygon = polygon();
        }
        return _cachedPolygon;
    }

    public ArrayList<TPoint> allVertexs() {
        ArrayList<TPoint> output = new ArrayList<>();
        output.add(new TPoint(xmin, ymin, zmin));

        output.add(new TPoint(xmax, ymin, zmin));
        output.add(new TPoint(xmin, ymax, zmin));
        output.add(new TPoint(xmin, ymin, zmax));

        output.add(new TPoint(xmax, ymax, zmin));
        output.add(new TPoint(xmin, ymax, zmax));
        output.add(new TPoint(xmax, ymin, zmax));

        output.add(new TPoint(xmax, ymax, zmax));

        return output;
    }

    public int compare(TVoxel voxel) {
        int LESS = -1, EQUAL = 0, GREAT = 1;
        if (zmin < voxel.zmin) {
            return LESS;
        } else if (zmin > voxel.zmin) {
            return GREAT;
        }

        if (ymin < voxel.ymin) {
            return LESS;
        } else if (ymin > voxel.ymin) {
            return GREAT;
        }

        if (xmin < voxel.xmin) {
            return LESS;
        } else if (xmin > voxel.xmin) {
            return GREAT;
        }

        return EQUAL;
    }

    @Override
    public String toString() {
        Float size = Math.abs(xmax - xmin) * Math.abs(ymax - ymin) * Math.abs(zmax - zmin);
        return String.format("TVoxel MIN(%f, %f, %f) MAX (%f, %f, %f), size= %f", xmin, ymin, zmin, xmax, ymax, zmax, size);
    }
}
