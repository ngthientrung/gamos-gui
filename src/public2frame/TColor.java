/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

import java.awt.Color;

/**
 *
 * @author dell
 */
public class TColor {
    public static TColor black = new TColor(0f, 0f, 0f);
    public static TColor white = new TColor(1f, 1f, 1f);
    public static TColor yellow = new TColor(0.33f, 0.5f, 0.7f);
    public float red = (float)0.0, green = (float)0.0, blue = (float)0.0;

    public TColor(Float r, Float g, Float b) {
        red = r;
        green = g;
        blue = b;
    }
    
    private byte boundValue(int value) {
        if(value < 0) { return 0; }
        if(value > 255) { return (byte) 255; }
        return (byte) value;
    }
    
    public byte redByte() {
        return boundValue((int) (red * 255));
    }
    
    public byte greenByte() {
        return boundValue((int) (green * 255));
    }
        
    public byte blueByte() {
        return boundValue((int) (blue * 255));
    }
    
    public Color color() {
        try {
            return new Color(red, green, blue);
        } catch (Exception e) {
            System.err.println("Red=" + red + ", Green = " + green + ", Blue = " + blue);
            return Color.white;
        }
        
    }

    public TColor(String line) {
        try {
            String[] parts = line.split(" ");
            red = Float.parseFloat(parts[0]);
            green = Float.parseFloat(parts[1]);
            blue = Float.parseFloat(parts[2]);
        } catch (Exception e) {
        }
    }
}
