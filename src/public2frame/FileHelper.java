/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import javax.swing.JFileChooser;

/**
 *
 * @author apple
 */
public class FileHelper {

    static String desktopPath() {
        String desktopPath = System.getProperty("user.home") + "/Desktop";
        return desktopPath.replace("\\", "/");
    }

    static File saveFileBrowser() {
        File path = new File(desktopPath());
        JFileChooser jfc = new JFileChooser(path);
        int returnValue = jfc.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            return jfc.getSelectedFile();
        }
        return null;
    }

    static File openFileBrowser() {
        File path = new File(desktopPath());
        JFileChooser jfc = new JFileChooser(path);
        int returnValue = jfc.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            return jfc.getSelectedFile();
        }
        return null;
    }

    static File openDirBrowser() {
        File path = new File(desktopPath());
        JFileChooser jfc = new JFileChooser(path);
        jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnValue = jfc.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            return jfc.getSelectedFile();
        }
        return null;
    }

//    public static ArrayList<String> linesOfFileUtf8(String filePath) {
//        File f = new File(filePath);
//        return linesOfFileUtf8(f);
//    }
//    
    public static ArrayList<String> linesOfFile(String filePath) {
        File f = new File(filePath);
        ArrayList<String> lines = FileHelper.linesOfFileUtf16(f);
        if (lines.size() == 1) {
            lines = FileHelper.linesOfFileUtf8(f);
        }
        return lines;
    }

    public static ArrayList<Integer> intergersInLine(String line) {
        ArrayList<Integer> output = new ArrayList<>();
        String parts[] = line.trim().split(" ");
        for (String p : parts) {
            try {
                Integer value = Integer.parseInt(p);
                output.add(value);
            } catch (NumberFormatException e) {
            }
        }
        return output;
    }

    public static ArrayList<String> partsOfLine(String line, String cutChars) {
        line = line.replaceAll(cutChars, "<cut>");
        String[] parts = line.split("<cut>");
        ArrayList<String> output = new ArrayList<>();
        for (String p : parts) {
            if (p.isEmpty()) {
                continue;
            }
            output.add(p.trim());
        }
        return output;
    }

    public static ArrayList<Float> FloatsInLine(String line) {
        ArrayList<Float> output = new ArrayList<>();
        String parts[] = line.trim().split(" ");
        for (String p : parts) {
            try {
                Float value = Float.parseFloat(p);
                output.add(value);
            } catch (NumberFormatException e) {
            }
        }
        return output;
    }

    public static ArrayList<Float> floatsInLine(String line) {
        ArrayList<Float> output = new ArrayList<>();
        String parts[] = line.trim().split(" ");
        for (String p : parts) {
            try {
                Float value = Float.parseFloat(p);
                output.add(value);
            } catch (NumberFormatException e) {
            }
        }
        return output;
    }

    public static ArrayList<String> linesOfFileUtf16(File f) {
        return linesOfFile(f, "UTF-16");
    }

    public static ArrayList<String> linesOfFileUtf8(File f) {
        return linesOfFile(f, "UTF-8");
    }

    public static ArrayList<String> linesOfFile(File f, String encoding) {
        ArrayList<String> lines = new ArrayList<>();
        try {

            BufferedReader breader = new BufferedReader(new InputStreamReader(new FileInputStream(f), encoding));
            StringBuilder stringBuilder = new StringBuilder();

            while (true) {
                String line = breader.readLine();
                if (line == null) {
                    break;
                }
                lines.add(line.trim());
            }

        } catch (IOException ex) {
        }
        return lines;
    }
}
