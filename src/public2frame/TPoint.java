/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

/**
 *
 * @author apple
 */
public class TPoint {

    public float x, y, z;
    public int color = 0x000000;

    public TPoint(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public TPoint(String line) {
        String[] originParts = line.trim().split(" ");

        x = Float.parseFloat(originParts[0]);
        y = Float.parseFloat(originParts[1]);
        z = Float.parseFloat(originParts[2]);
    }

    public int compare(TPoint point) {
        int LESS = -1, EQUAL = 0, GREAT = 1;
        if (z < point.z) {
            return LESS;
        } else if (z > point.z) {
            return GREAT;
        }

        if (y < point.y) {
            return LESS;
        } else if (y > point.y) {
            return GREAT;
        }

        if (x < point.x) {
            return LESS;
        } else if (x > point.x) {
            return GREAT;
        }

        return EQUAL;
    }

    public Float khoangCach(TPoint point2) {
        Float x2 = (float) Math.pow(point2.x - x, 2);
        Float y2 = (float) Math.pow(point2.y - y, 2);
        Float z2 = (float) Math.pow(point2.z - z, 2);
        return (float) Math.sqrt(x2 + y2 + z2);
    }

    @Override
    public String toString() {
        return String.format("TPoint (x=%f, y=%f, z=%f)", x, y, z);
    }

    public String lineString() {
        return String.format("%f %f %f", x, y, z);
    }

    public String lineString2() {
        return String.format("(%f %f %f)", x, y, z);
    }
}
