/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import org.locationtech.jts.geom.LinearRing;

/**
 *
 * @author apple
 */
public class Dicom2G4Reader {

    public static class Data {

        public float zMin = Float.MAX_VALUE;
        public float zMax = Float.MIN_VALUE;
        public float dz = Float.MIN_VALUE;
        public final ArrayList<ArrayList<Bound>> layers = new ArrayList<>();

        public ArrayList<Bound> boundAt(Float z, Float min, Float max, float zWeight) {
            float boundStart = Float.MIN_VALUE;
            float boundEnd = Float.MAX_VALUE;
            for (float zi = min; zi < max; zi += zWeight) {
                if (zi <= z && z < zi + zWeight) {
                    boundStart = zi;
                    boundEnd = zi + zWeight;
                    break;
                }
            }

            ArrayList<Bound> output = new ArrayList<>();

            if (boundStart == Float.MIN_VALUE || boundEnd == Float.MAX_VALUE) {
                return output;
            }

            int size = layers.size();
            for (int i = 0; i < size - 1; i++) {
                ArrayList<Bound> current = layers.get(i);
                if (boundStart <= (current.get(0).z + zWeight) && (current.get(0).z + zWeight) < boundEnd) {
                    String s = String.format("SliceZ:%f, boundStart:%f, boundEnd:%f, bourderZ:%f", z, boundStart, boundEnd, current.get(0).z);
                    System.out.println(s);
                    return current;
                }
            }

            return output;
        }
    }

    public static class Bound {

        public String name = null;
        public int indexBoundInContour = 0;
        public Float z = 0f;
        public ArrayList<TPoint> points = new ArrayList<>();

        public boolean isSameAsFirstPoint(TPoint p) {
            if (points.isEmpty()) {
                return false;
            }
            TPoint pi = points.get(0);
            return (pi.x == p.x && pi.y == p.y && p.z == pi.z);
        }
        
        public LinearRing linearRing() {
            return Util.linearRingFrom(points);
        }
    }

    public static Data read(String path) {
        namesLv2.clear();
        File f = new File(path);
        final String startRead = "@@@ GeomType CLOSED_PLANAR";
//        final String lineName = "@@@@ CONTOUR ";
        final String lineTenCauTrucMarker = "@@@@@ ROI: ";
        Data data = new Data();
        try {
            String tenCauTruc = "";
            boolean isRead = false;
            ArrayList<Bound> layer = new ArrayList<>();
            Bound bound = new Bound();
            TPoint endPoint = null;
            BufferedReader breader = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
            while (true) {
                String line = breader.readLine();
                if (line == null) {
                    break;
                }

                if(line.contains(lineTenCauTrucMarker)) {
                    tenCauTruc = line.replace(lineTenCauTrucMarker, "").replace(" ", "");
                    continue;
                }

                if (line.contains(startRead)) {
                    isRead = true;
                    layer = new ArrayList<>();
                    data.layers.add(layer);
                    bound = new Bound();
                    layer.add(bound);
                    updateZInfo(bound, data, layer);
                    continue;
                }

                if (line.contains("DicomFileMgr")) {
                    break;
                }

                if (!isRead) {
                    continue;
                }

                TPoint point = point(line);

                if (point == null) {
                    continue;
                }

                if (endPoint != null
                        && endPoint.x == point.x
                        && endPoint.y == point.y
                        && endPoint.z == point.z) {
                    continue;
                }

                if (bound.isSameAsFirstPoint(point)) {
                    bound.points.add(point);
                    bound.z = point.z;
                    endPoint = point;

                    bound = new Bound();

                    layer.add(bound);
                    updateZInfo(bound, data, layer);
                } else {
                    if (bound.points.isEmpty() && bound.name == null) {
                        String name = getFullName(tenCauTruc, bound, point.z);
                        if(name != null) {
                            bound.name = name;
                        }
                    }
                    bound.points.add(point);
                    bound.z = point.z;
                    endPoint = point;
                    addBoundIfNeeded(layer, bound);
                    updateZInfo(bound, data, layer);
                }
            }
        } catch (FileNotFoundException ex) {
        } catch (UnsupportedEncodingException ex) {
        } catch (IOException ex) {
        }
        return data;
    }
    static ArrayList<String> namesLv1 = new ArrayList<>();
    static ArrayList<String> namesLv2 = new ArrayList<>();
    
    private static void updateZInfo(Bound newBound, Data data, ArrayList<Bound> layer) {
        data.zMin = Float.min(newBound.z, data.zMin);
        data.zMax = Float.max(newBound.z, data.zMax);
        
        if(layer.size() >= 2) {
            data.dz = layer.get(1).z - layer.get(0).z;
        }
    }
    
    private static void addBoundIfNeeded(ArrayList<Bound> layer, Bound bound) {
        if(layer.contains(bound)) { return; }
        layer.add(bound);
    }
    
//    private static String getTenCauTruc(String line) {
//        return line.split(" ")[0];
//    }

//    private static void addNameLv1(String name) {
//        name = name.toUpperCase().replace(" ", "_");
//        namesLv1.add(name);
//    }
    private static final ArrayList<Bound> nameMarkedBounds = new ArrayList<>();
    private static String getFullName(String name, Bound bound, float z) {
        for(Bound b: nameMarkedBounds) {
            if(b == bound) {
                return null;
            }
        }
        int count = 0;
        namesLv2.add(name);
        for (String n : namesLv2) {
            if (name.equals(n)) {
                count++;
            }
        }
        
        String zName = String.format("_z%.2f", z);
        zName = zName.replace("-", "x");
        nameMarkedBounds.add(bound);
        name = name + "_" + count;
        if(!name.contains("_z")) {
            name += zName;
        }
        return name;
    }
//    private String structName(String popularValue) {
//        final String name = popularValue.toUpperCase().replace(" ", "");
//        namesLv1.add(name);
//        int count = 0;
//        for (String n : namesLv1) {
//            if (n.contentEquals(name)) {
//                count++;
//            }
//        }
//        String nameLv1 = name + "_" + count;
//        count = 0;
//        return name + "_" + count;
//    }

    private static TPoint point(String line) {
        if (!line.startsWith("(")) {
            return null;
        }
        String[] parts = line.replace("(", "").replace(")", "").split(",");
        try {
            Float x = Float.parseFloat(parts[0]);
            Float y = Float.parseFloat(parts[1]);
            Float z = Float.parseFloat(parts[2]);
            return new TPoint(x, y, z);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
