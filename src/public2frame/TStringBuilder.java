/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

/**
 *
 * @author apple
 */
public class TStringBuilder {
    private final StringBuilder builder = new StringBuilder();
    
    void appendLine(String format, Object... args) {
        String string = String.format(format + "\n", args);
        builder.append(string);
    }

    @Override
    public String toString() {
        return builder.toString();
    }
    
    
}
