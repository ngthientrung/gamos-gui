/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import org.locationtech.jts.geom.Polygon;

/**
 *
 * @author apple
 */
public class GeomReader {

    public static class Solid implements Cloneable {

        static final float sizeOfBox = 0.1f;
        public String type;
        public String name;
        public String combineName;
        public String structure;
        public int numberOfVertex;

        public String bigSolidName, smallSolidName;

        public ArrayList<TPoint> points = new ArrayList<>();

        public float xmin = 999999, xmax = -999999, ymin = 999999, ymax = -999999;
        public float z1;
        public float z2;
        public TypeOfSolid typeOfSolid = TypeOfSolid.None;
        public Solid father = null;
        public int indexOfSon = 0;
        public int numberOfSon = 0;

        public static enum TypeOfSolid {
            Outside, Inside, None
        }

        private static int cloneId = 0;

        private String cloneName() {
            System.out.println("name = " + name);
            String real = Util.getStringBefore(name, "_id");
            String id = String.format("%07d", cloneId++);
            return real + "_id" + id;
        }

        public Solid clone(ArrayList<TPoint> newPoints) {
            Solid newSolid = new Solid();
            newSolid.type = type;
            newSolid.name = cloneName();
            newSolid.combineName = combineName;
            newSolid.structure = structure;
            newSolid.numberOfVertex = newPoints.size();

            newSolid.points = newPoints;
            Float tempXmin = 0f, tempXmax = 0f, tempYmin = 0f, tempYmax = 0f;
            findMinMax(newPoints, tempXmin, tempXmax, tempYmin, tempYmax);
            newSolid.xmin = tempXmin;
            newSolid.xmax = tempXmax;
            newSolid.ymin = tempYmin;
            newSolid.ymax = tempYmax;
            newSolid.z1 = z1;
            newSolid.z2 = z2;

            return newSolid;
        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            return super.clone(); //To change body of generated methods, choose Tools | Templates.
        }

        public static void findMinMax(ArrayList<TPoint> points, Float xmin, Float xmax, Float ymin, Float ymax) {
            int numberOfVertex = points.size();
            xmin = 999999f;
            xmax = -999999f;
            ymin = 999999f;
            ymax = -999999f;
            for (int i = 0; i < numberOfVertex; i++) {
                float x = points.get(i).x;
                float y = points.get(i).y;

                xmin = Float.min(xmin, x);
                xmax = Float.max(xmax, x);

                ymin = Float.min(ymin, y);
                ymax = Float.max(ymax, y);
            }
        }

    }

    public ArrayList<Solid> readSolid(String path, int nMinZ) {
        ArrayList<Solid> output = new ArrayList<>();
        File f = new File(path);
        try {
            BufferedReader breader = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
            while (true) {
                String line = breader.readLine();
                if (line == null) {
                    break;
                }

                if (line.contains(":PLACE")) {
                    continue;
                }

                if (line.contains(" SUBTRACTION ")) {
                    continue;
                }

                if (line.contains("_vs_")) {
                    continue;
                }

                if (line.contains(" BOX ")) {
                    continue;
                }

                if (line.isEmpty()) {
                    continue;
                }
                String[] parts = line.split(" ");
                int partIndex = 0;
                String type = parts[partIndex++]; //:SOLID
                String name = parts[partIndex++]; // %s%d
                partIndex++; //EXTRUDED
                String structure = Util.removeNumberIn(name);
                int numberOfVertex = Integer.parseInt(parts[partIndex++]); // NUMBER POINTS
                ArrayList<TPoint> points = new ArrayList<>();
                float xmin = 999999, xmax = -999999, ymin = 999999, ymax = -999999;
                for (int i = 0; i < numberOfVertex; i++) {
                    float x = Float.parseFloat(parts[partIndex++]); // X
                    float y = Float.parseFloat(parts[partIndex++]); // Y
                    points.add(new TPoint(x, y, 0));

                    xmin = Float.min(xmin, x);
                    xmax = Float.max(xmax, x);

                    ymin = Float.min(ymin, y);
                    ymax = Float.max(ymax, y);
                }
                partIndex++; // 2 or 3
                float z1 = Float.parseFloat(parts[partIndex++]); // z1
                partIndex++; // 0
                partIndex++; // 0
                partIndex++; // 1
                if (nMinZ == 3) {
                    partIndex += 4;
                }
                float z2 = Float.parseFloat(parts[partIndex++]); // z2
                partIndex++; // 0
                partIndex++; // 0
                partIndex++; // 1

                String zName = String.format("_z%.2f", (z1 + z2) / 2);
                if (!name.contains("_z")) {
                    name += zName;
                }

                Solid solid = new Solid();
                solid.name = name;
                solid.combineName = name;
                solid.numberOfVertex = numberOfVertex;
                solid.points = points;
                solid.structure = structure;
                solid.type = type;
                solid.z1 = z1;
                solid.z2 = z2;

                solid.xmin = xmin;
                solid.xmax = xmax;

                solid.ymin = ymin;
                solid.ymax = ymax;

                for (TPoint p : solid.points) {
                    p.z = z1;
                }
                Util.clockwise(solid.points);
                output.add(solid);

            }

        } catch (FileNotFoundException ex) {
        } catch (UnsupportedEncodingException ex) {
        } catch (IOException ex) {
        }

        return output;
    }

    public final class Volu {

        public final String name;
        public final ArrayList<TPoint> points;
        public ArrayList<TVoxel> voxels = new ArrayList<>();
        public final float zMin, zMax;
        boolean isSubRing = false;

        public Volu(String name, ArrayList<TPoint> points, float zMin, float zMax) {
            this.name = name;
            this.points = points;
            this.zMin = zMin;
            this.zMax = zMax;
            this.points.add(points.get(0));
        }

        public int getStructureIndex() {
            return Util.getStructIndexFromStructName(name);
        }

        public boolean firstCharIsNumber(String s) {
            String firstChar = s.substring(0, 1);
            return firstChar.chars().allMatch(Character::isDigit);
        }

        public boolean isSameZLayer(TVoxel voxel) {
            boolean isSame = (voxel.zmin >= zMin) && (voxel.zmax <= zMax);
            return isSame;
        }

        public boolean isIntersect(TVoxel voxel) {
            if (!isSameZLayer(voxel)) {
                return false;
            }

            return cachedPolygon().intersects(voxel.cachedPolygon()) || (cachedPolygon().touches(voxel.cachedPolygon()) && voxel.cachedPolygon().within(cachedPolygon()));
        }

        ArrayList<Volu> rings = new ArrayList<>();

        public Polygon polygon() {
            ArrayList<ArrayList<TPoint>> holePoints = new ArrayList<>();
            for (int hpi = 0; hpi < rings.size(); hpi++) {
                rings.get(hpi).points.add(rings.get(hpi).points.get(0));
                holePoints.add(rings.get(hpi).points);
            }
            System.out.println(name);
            return Util.polygonFrom(points, holePoints);
        }

        Polygon _cachedPolygon = null;

        public Polygon cachedPolygon() {
            if (_cachedPolygon == null) {
                _cachedPolygon = polygon();
            }
            return _cachedPolygon;
        }
    }

    public ArrayList<Volu> readVolu(String path, int nMinZ) {
        ArrayList<Volu> output = new ArrayList<>();
        ArrayList<String> lines = FileHelper.linesOfFile(path);
        for (String line : lines) {
            Util.log("Read Geom: %s", line);
            String[] parts = line.split(" ");
            boolean isSolidOrVolu = parts[0].contains(":VOLU") || parts[0].contains(":SOLID");
            if (isSolidOrVolu && !line.contains("_vs_") && !line.contains("_VS_") && !line.contains("SUBTRACTION")) {
                ArrayList<TPoint> points = new ArrayList<>();
                String name = parts[1];
                int numberOfPoint = Integer.parseInt(parts[3]);
                int index = 4;
                for (int i = 0; i < numberOfPoint; i++) {
                    float x = Float.parseFloat(parts[index++]);
                    float y = Float.parseFloat(parts[index++]);

                    points.add(new TPoint(x, y, 0));
                }
                index++; // 3
                float zMin = Float.parseFloat(parts[index++]);
                index++; // 0
                index++; // 0
                index++; // 1
                if (nMinZ == 3) {
                    index += 4;
                }
                float zMax = Float.parseFloat(parts[index++]);
                index++; // 0
                index++; // 0
                index++; // 1

                output.add(new Volu(name, points, zMin, zMax));
            }

            if (parts[0].contains(":SOLID") && line.contains("SUBTRACTION")) {
                String nameBigSolid = parts[3];
                String nameSmallSolid = parts[4];

                nameBigSolid = nameBigSolid.split("_vs_")[0];

                Volu bigVolu = findVolu(nameBigSolid, output);
                Volu insideVolu = findVolu(nameSmallSolid, output);
                insideVolu.isSubRing = true;
                bigVolu.rings.add(insideVolu);
            }
        }

        return voluNotRing(output);
    }

    Volu findVolu(String name, ArrayList<Volu> listVolus) {
        for (Volu v : listVolus) {
            if (v.name.equals(name)) {
                return v;
            }
        }

        return null;
    }

    ArrayList<Volu> voluNotRing(ArrayList<Volu> listVolus) {
        ArrayList<Volu> output = new ArrayList<>();
        for (Volu v : listVolus) {
            if (!v.isSubRing) {
                output.add(v);
            }
        }
        return output;
    }
}
