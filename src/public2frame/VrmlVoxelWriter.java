/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apple
 */
public class VrmlVoxelWriter {
    private final TStringBuilder builder = new TStringBuilder();
    private final String voxelName, fileName;

    public VrmlVoxelWriter(String voxelName, String fileName) {
        this.voxelName = voxelName;
        this.fileName = fileName;
    }

    public void startWrite(ArrayList<TVoxel> voxels) {
        try {
            File file = new File(fileName);
//            FileWriter writer = new FileWriter(file);
            //new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-16"));
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-16"));
            
            writeHeader(bufferedWriter);
            
            int index = 0;
            for(TVoxel v: voxels) {
                String name = voxelName + "_" + index++;
                writeVoxel(bufferedWriter, v, name);
            }
            
            writeFooter(bufferedWriter);
            
            bufferedWriter.close();
//            writer.close();
            
        } catch (IOException e) {}

    }
    
    void writeHeader(BufferedWriter bufferedWriter) {
        writeLine(bufferedWriter, "#VRML V2.0 utf8");
        writeLine(bufferedWriter, "Viewpoint {");
        writeLine(bufferedWriter, "     orientation 0 0 1 0.75");
        writeLine(bufferedWriter, "     position 0 0 0");
        writeLine(bufferedWriter, "}");
        writeLine(bufferedWriter, "Transform {");
        writeLine(bufferedWriter, "     children [");
    }
    
    void writeFooter(BufferedWriter bufferedWriter) {
        writeLine(bufferedWriter, "     ]");
        writeLine(bufferedWriter, "}");
    }
    
    void writeLine(BufferedWriter bufferedWriter, String formatText, Object... args) {
        try {
            String string = String.format(formatText + "\n", args);
            bufferedWriter.write(string);
        } catch (IOException ex) {
            Logger.getLogger(VrmlVoxelWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void writeVoxel(BufferedWriter bufferedWriter, TVoxel voxel, String name) {
        writeLine(bufferedWriter, "               DEF \"%s\" Transform {", name);
        writeLine(bufferedWriter, "               translation     0 0 0");
        writeLine(bufferedWriter, "               children [");
        writeLine(bufferedWriter, "                    Shape { appearance Appearance { material Material { diffuseColor %f %f %f transparency 1}}", voxel.color.red, voxel.color.green, voxel.color.blue);
        writeLine(bufferedWriter, "                         geometry PointSet {");
        writeLine(bufferedWriter, "                              coord Coordinate {");
        writeLine(bufferedWriter, "                                   point [");
        writeLine(bufferedWriter, "                                   %f %f %f", voxel.xmin, voxel.ymin, voxel.zmin);
        writeLine(bufferedWriter, "                                   %f %f %f", voxel.xmin, voxel.ymin, voxel.zmax);
        writeLine(bufferedWriter, "                                   %f %f %f", voxel.xmin, voxel.ymax, voxel.zmax);
        writeLine(bufferedWriter, "                                   %f %f %f", voxel.xmin, voxel.ymax, voxel.zmin);
        
        writeLine(bufferedWriter, "                                   %f %f %f", voxel.xmax, voxel.ymax, voxel.zmax);
        writeLine(bufferedWriter, "                                   %f %f %f", voxel.xmax, voxel.ymax, voxel.zmin);
        writeLine(bufferedWriter, "                                   %f %f %f", voxel.xmax, voxel.ymin, voxel.zmin);
        writeLine(bufferedWriter, "                                   %f %f %f", voxel.xmax, voxel.ymin, voxel.zmax);
        writeLine(bufferedWriter, "                                   ]");
        writeLine(bufferedWriter, "                              }");
        writeLine(bufferedWriter, "                              coordIndex [");
        writeLine(bufferedWriter, "                                   0 3 5 6 -1,");
        writeLine(bufferedWriter, "                                   0 1 2 3 -1,");
        writeLine(bufferedWriter, "                                   0 1 7 6 -1,");
        writeLine(bufferedWriter, "                                   4 2 3 5 -1,");
        writeLine(bufferedWriter, "                                   4 5 6 7 -1,");
        writeLine(bufferedWriter, "                                   4 7 1 2 -1,");
        writeLine(bufferedWriter, "                              ]");
        writeLine(bufferedWriter, "                         }");
        writeLine(bufferedWriter, "                    }");
        writeLine(bufferedWriter, "               ]");
        writeLine(bufferedWriter, "          }");
    }
    
}
