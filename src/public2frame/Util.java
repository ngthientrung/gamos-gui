/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

import public2frame.G4DcmReader;
import public2frame.GeomWriter;
import public2frame.VrmlVoxelWriter;
import public2frame.TBound;
import public2frame.TVoxel;
import java.awt.Component;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import org.apache.commons.io.FileUtils;
import org.locationtech.jts.algorithm.Angle;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.impl.CoordinateArraySequenceFactory;

/**
 *
 * @author dell
 */
public class Util {

    public static void log(String format, Object... objs) {
        System.out.println(String.format(format, objs));
    }

    public static ArrayList<Float> floatsInLine(String line) {
        ArrayList<Float> output = new ArrayList<>();
        String parts[] = line.trim().split(" ");
        for (String p : parts) {
            try {
                Float value = Float.parseFloat(p);
                output.add(value);
            } catch (Exception e) {
            }
        }
        return output;
    }

    public static ArrayList<Integer> intergersInLine(String line) {
        ArrayList<Integer> output = new ArrayList<>();
        String parts[] = line.trim().split(" ");
        for (String p : parts) {
            try {
                Integer value = Integer.parseInt(p);
                output.add(value);
            } catch (Exception e) {
            }
        }
        return output;
    }

    public static double intersectArea(Polygon polygon1, Polygon polygon2) {
        return polygon1.intersection(polygon2).getArea();
    }

    public static boolean isContainStrings(ArrayList<String> objs, String ox) {
        for (Object obj : objs) {
            if (obj.equals(ox)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isContain(ArrayList<TPoint> points, TPoint p) {
        for (TPoint point : points) {
            if (point.compare(p) == 0) {
                return true;
            }
        }
        return false;
    }

    public static int count(ArrayList<Integer> nums, Integer num) {
        int count = 0;
        for (Integer n : nums) {
            if (n.equals(num)) {
                count++;
            }
        }
        return count;
    }

    public static ArrayList<TPoint> points(Polygon polygon) {
        ArrayList<TPoint> output = new ArrayList<>();
        Coordinate[] coords = polygon.getCoordinates();
        for (Coordinate c : coords) {
            TPoint newTpoint = new TPoint((float) c.x, (float) c.y, (float) c.z);
//            if (Util.isContain(output, newTpoint)) {
//                continue;
//            }
            output.add(newTpoint);
        }

        return output;
    }

    public static ArrayList<Integer> readIntegersInline(BufferedReader reader) {
        try {
            return intergersInLine(reader.readLine());
        } catch (IOException ex) {
            return null;
        }
    }

    public static ArrayList<Float> readFloatsInline(BufferedReader reader) {
        try {
            return floatsInLine(reader.readLine());
        } catch (IOException ex) {
            return null;
        }
    }

    public static BufferedReader openReader(String path) {
        try {
            File f = new File(path);
            return new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
        }
        return null;
    }
    
    public static void print(String path, ArrayList<Object[]> extrLines) {
        try {
            BufferedWriter writer = Util.writer(path);
            for(Object[] objs: extrLines) {
                String line = "";
                for(Object o: objs) {
                    line += " " + o;
                }
                line = line.trim();
                line += "\n";
                writer.write(line);
            }
            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static BufferedWriter openWriter(String path) {
        try {
            File f = new File(path);
            return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
        }
        return null;
    }

    public static void closeReader(BufferedReader reader) {
        try {
            reader.close();
        } catch (Exception e) {
        }
    }

    public static class Index3Creator {

        final int nx, ny, nz;
        private int count = 0;

        public Index3Creator(int nx, int ny, int nz) {
            this.nx = nx;
            this.ny = ny;
            this.nz = nz;
        }

        public Index3 next() {
            Index3 i3 = new Index3(
                    Util.indexX(count, nx),
                    Util.indexY(count, nx, ny, nz),
                    Util.indexZ(count, nx, ny, nz)
            );
            count++;
            return i3;
        }

        public Boolean isFull() {
            return count >= (nx * ny * nz);
        }
    }

    public static class Index3 {

        public final int xi, yi, zi;

        public Index3(int xi, int yi, int zi) {
            this.xi = xi;
            this.yi = yi;
            this.zi = zi;
        }
    }

    public static String subStringBetweenText(String text, String startText, String endText) {
        int startIndex = text.indexOf(startText) + startText.length();
        int endIndex = text.indexOf(endText);

        if (startIndex == -1 || endIndex == -1) {
            return null;
        }

        return text.substring(endIndex, endIndex);
    }

    public static Float maxIn(ArrayList<Float> numbers) {
        Float max = Float.MIN_VALUE;
        for (Float number : numbers) {
            max = Math.max(number, max);
        }
        return max;
    }

    public static Float minIn(ArrayList<Float> numbers) {
        Float min = Float.MAX_VALUE;
        for (Float number : numbers) {
            min = Math.min(number, min);
        }
        return min;
    }

    public static void debugPrint(Object[][][] arr, int nx, int ny, int nz) {
        for(int zi = 0; zi < nz; zi++) {
            for(int yi = 0; yi < ny; yi++) {
                String line = "";
                for(int xi = 0; xi < nx; xi++) {
                    line += " " + arr[xi][yi][zi];
                }
                System.out.println(line.trim());
            }
            System.out.println("--------------------");
        }
    }
    
    public static String pathValue(JLabel label) {
        if ("-".equals(label.getText()) || label.getText() == null) {
            return null;
        }

        return label.getText();
    }

    public static void showDirDialogAndSetPath(JLabel label) {
        File file = FileHelper.openDirBrowser();
        if (file != null) {
            label.setText(file.getAbsolutePath());
        }
    }

    public static void showFileDialogAndSetPath(JLabel label) {
        File file = FileHelper.openFileBrowser();
        if (file != null) {
            label.setText(file.getAbsolutePath());
        }
    }

    public static void showFileForSaveDialogAndSetPath(JLabel label) {
        File file = FileHelper.saveFileBrowser();
        if (file != null) {
            label.setText(file.getAbsolutePath());
        }
    }

    public static Boolean isValidPath(Component component, String labelText, String message) {
        if ("-".equals(labelText) || labelText == null) {
            JOptionPane.showMessageDialog(component, message);
            return false;
        }
        return true;
    }

    public static Boolean isValidFloat(Component component, String text, String message) {
        try {
            Float.parseFloat(text);
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(component, message);
            return false;
        }
    }

    public static void showCompleteMessage(Component component) {
        JOptionPane.showMessageDialog(component, "SUCCESS");
    }

    public static void showMessageDialog(Component component, String message) {
        JOptionPane.showMessageDialog(component, message);
    }

    public static Rectangle rectangleToFit(Size boundSize, Size originSize) {
        Float tileBound = (float) boundSize.width / (float) boundSize.height;
        Float tileOrigin = (float) originSize.width / (float) originSize.height;

        if (tileBound > tileOrigin) {
            float h = (float) boundSize.height;
            float w = (int) (h * (float) originSize.width / (float) originSize.height);
            float y = 0;
            float x = (boundSize.width - w) / 2;
            return new Rectangle(x, y, w, h);
        } else {
            float w = (float) boundSize.width;
            float h = (int) (w * (float) originSize.height / (float) originSize.width);
            float x = 0;
            float y = (boundSize.height - h) / 2;
            return new Rectangle(x, y, w, h);
        }
    }

    public static Coordinate coordinate(TPoint p) {
        return new Coordinate(p.x, p.y, p.z);
    }

    public static double angleAOB(TPoint pA, TPoint pO, TPoint pB) {

        double angleRad = Angle.angleBetween(coordinate(pA),
                coordinate(pO),
                coordinate(pB));
        return Angle.toDegrees(angleRad);
    }

    public static class Rectangle {

        public final Size size;
        public final int x;
        public final int y;

        public Rectangle(Size size, int x, int y) {
            this.size = size;
            this.x = x;
            this.y = y;
        }

        public Rectangle(int x, int y, int w, int h) {
            this.size = new Size(w, h);
            this.x = x;
            this.y = y;
        }

        public Rectangle(float x, float y, float w, float h) {
            this.size = new Size(w, h);
            this.x = (int) x;
            this.y = (int) y;
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    public static class Size {

        public final int width;
        public final int height;

        public Size(int width, int height) {
            this.width = width;
            this.height = height;
        }

        private Size(float w, float h) {
            this.width = (int) w;
            this.height = (int) h;
        }
    }

    public static Float[][][] Floats(Integer[][][] input, int n1, int n2, int n3) {
        Float[][][] output = new Float[n1][n2][n3];
        for (int i1 = 0; i1 < n1; i1++) {
            for (int i2 = 0; i2 < n2; i2++) {
                for (int i3 = 0; i3 < n3; i3++) {
                    output[i1][i2][i3] = (float) input[i1][i2][i3];
                }
            }
        }
        return output;
    }

    public static Float[][][] floats(Integer[][][] input, int n1, int n2, int n3) {
        Float[][][] output = new Float[n1][n2][n3];
        for (int i1 = 0; i1 < n1; i1++) {
            for (int i2 = 0; i2 < n2; i2++) {
                for (int i3 = 0; i3 < n3; i3++) {
                    output[i1][i2][i3] = input[i1][i2][i3].floatValue();
                }
            }
        }
        return output;
    }

    public static Float max(Float[][][] input, int n1, int n2, int n3) {
        Float output = Float.MIN_VALUE;
        for (int i1 = 0; i1 < n1; i1++) {
            for (int i2 = 0; i2 < n2; i2++) {
                for (int i3 = 0; i3 < n3; i3++) {
                    output = Math.max(input[i1][i2][i3], output);
                }
            }
        }
        return output;
    }

    public static Integer max(Integer... values) {
        Integer output = Integer.MIN_VALUE;
        for (int i = 0; i < values.length; i++) {
            output = Math.max(output, values[i]);
        }
        return output;
    }

    public static Float max(Float... values) {
        Float output = Float.MIN_VALUE;
        for (int i = 0; i < values.length; i++) {
            output = Math.max(output, values[i]);
        }
        return output;
    }

    public static Float min(Float... values) {
        Float output = Float.MAX_VALUE;
        for (int i = 0; i < values.length; i++) {
            output = Math.min(output, values[i]);
        }
        return output;
    }

    public static Float maxX(TPoint... points) {
        Float output = Float.MIN_VALUE;
        for (int i = 0; i < points.length; i++) {
            output = Math.max(output, points[i].x);
        }
        return output;
    }

    public static Float minX(TPoint... points) {
        Float output = Float.MAX_VALUE;
        for (int i = 0; i < points.length; i++) {
            output = Math.min(output, points[i].x);
        }
        return output;
    }

    public static Float maxY(TPoint... points) {
        Float output = Float.MIN_VALUE;
        for (int i = 0; i < points.length; i++) {
            output = Math.max(output, points[i].y);
        }
        return output;
    }

    public static Float minY(TPoint... points) {
        Float output = Float.MAX_VALUE;
        for (int i = 0; i < points.length; i++) {
            output = Math.min(output, points[i].y);
        }
        return output;
    }

    public static Float maxZ(TPoint... points) {
        Float output = Float.MIN_VALUE;
        for (int i = 0; i < points.length; i++) {
            output = Math.max(output, points[i].z);
        }
        return output;
    }

    public static Float minZ(TPoint... points) {
        Float output = Float.MAX_VALUE;
        for (int i = 0; i < points.length; i++) {
            output = Math.min(output, points[i].z);
        }
        return output;
    }

    public static Float min(Float[][][] input, int n1, int n2, int n3) {
        Float output = Float.MAX_VALUE;
        for (int i1 = 0; i1 < n1; i1++) {
            for (int i2 = 0; i2 < n2; i2++) {
                for (int i3 = 0; i3 < n3; i3++) {
                    output = Math.min(input[i1][i2][i3], output);
                }
            }
        }
        return output;
    }

    // METHOD WRITE FILE
    public static void writeLine(BufferedWriter bufferedWriter, Integer... args) {
        String line = "";
        for (int i = 0; i < args.length; i++) {
            line += args[i] + " ";
        }
        writeLine(bufferedWriter, line.trim());
    }

    public static void writeLine(BufferedWriter bufferedWriter, Float... args) {
        String line = "";
        for (int i = 0; i < args.length; i++) {
            line += args[i] + " ";
        }
        writeLine(bufferedWriter, line.trim());
    }

    public static void writeLine(BufferedWriter bufferedWriter, String formatText, Object... args) {
        try {
            String string = String.format(formatText + "\n", args);
            bufferedWriter.write(string);
        } catch (IOException ex) {
            Logger.getLogger(VrmlVoxelWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static int integerFromTextField(JTextField field, int defaultValue) {
        try {
            return Integer.parseInt(field.getText());
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static float floatFromTextField(JTextField field, float defaultValue) {
        try {
            return Float.parseFloat(field.getText());
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static void writeLineWithFormat(BufferedWriter bufferedWriter, String format, Object... objects) {
        try {
            String line = "";
            for (Object o : objects) {
                line += String.format(format, o) + " ";
            }
            Util.log(line);
            bufferedWriter.write(line.trim() + "\n");
        } catch (IOException ex) {
            Logger.getLogger(VrmlVoxelWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void writeLineSplitByTab(BufferedWriter bufferedWriter, Object... objects) {
        try {
            String line = "";
            for (Object o : objects) {
                line += o.toString() + "\t";
            }
            Util.log(line);
            bufferedWriter.write(line.trim() + "\n");
        } catch (IOException ex) {
            Logger.getLogger(VrmlVoxelWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void writeLine(BufferedWriter bufferedWriter, Object... objects) {
        try {
            String line = "";
            for (Object o : objects) {
                line += o.toString() + " ";
            }
            Util.log(line);
            bufferedWriter.write(line.trim() + "\n");
        } catch (IOException ex) {
            Logger.getLogger(VrmlVoxelWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void writeLine(String path, Object... objects) {
        try {
            BufferedWriter writer = writer(path);
            writeLine(writer, objects);

            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void writeArray3(String filePath, Object[][][] objectsXYZ) {
        try {
            BufferedWriter writer = writer(filePath);
            writeArray3(writer, objectsXYZ);
            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void writeArray3(BufferedWriter bufferedWriter, Object[][][] objectsXYZ) {
        int nx = objectsXYZ.length;
        int ny = objectsXYZ[0].length;
        int nz = objectsXYZ[0][0].length;
        for (int iz = 0; iz < nz; iz++) {
            for (int iy = 0; iy < ny; iy++) {
                String line = "";
                for (int ix = 0; ix < nx; ix++) {
                    if (objectsXYZ[ix][iy][iz].toString().equals("0.0")) {
                        line += "0" + " ";
                    } else {
                        line += objectsXYZ[ix][iy][iz] + " ";
                    }
                }
                writeLine(bufferedWriter, line.trim());
            }
        }

    }

    public static class Point2D {

        final short w, h;

        public Point2D(short w, short h) {
            this.w = w;
            this.h = h;
        }

        @Override
        public String toString() {
            return "P:(" + w + "," + h + ")";
        }
    }

    public static Point2D translationPoint(Point2D originPoint, Util.Rectangle originRect, Util.Rectangle newRect) {
        float scaleW = (float) newRect.size.width / (float) originRect.size.width;
        float scaleH = (float) newRect.size.height / (float) originRect.size.height;

        float newW = newRect.x + (originPoint.w - originRect.x) * scaleW;
        float newH = newRect.y + newRect.size.height - (originPoint.h - originRect.y) * scaleH;

        return new Point2D((short) newW, (short) newH);
    }

    public static void savePNG(final BufferedImage bi, final String path) {
        try {
            RenderedImage rendImage = bi;
            ImageIO.write(rendImage, "PNG", new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int indexX(int numberIndex, int nx) {
        return numberIndex % nx;
    }

    public static int indexY(int numberIndex, int nx, int ny, int nz) {
        return (numberIndex / nx) % ny;
    }

    public static int indexZ(int numberIndex, int nx, int ny, int nz) {
        return (numberIndex / nx) / ny;
    }

    public static String stringFrom(List<Object> objects) {
        String s = "";
        s = objects.stream().map((obj) -> obj + " ").reduce(s, String::concat);
        return s.trim();
    }

    public static String stringFromWithTab(List<Object> objects) {
        String s = "";
        s = objects.stream().map((obj) -> obj + "\t").reduce(s, String::concat);
        return s.trim();
    }

    public static String stringFromTab(List<Object> objects) {
        String s = "";
        s = objects.stream().map((obj) -> obj + "\t").reduce(s, String::concat);
        return s.trim();
    }

    public static String removeNumberIn(String text) {
//        String[] nums = new String[10];
        String copy = text;
        for (int i = 0; i < 10; i++) {
            String num = i + "";
            copy = copy.replace(num, "");
        }
        return copy;

    }

    public static TBound bound(ArrayList<TPoint> points) {
        Float xmin, xmax, ymin, ymax, zmin, zmax;
        xmin = ymin = zmin = Float.MAX_VALUE;
        xmax = ymax = zmax = Float.MIN_VALUE;

        for (int i = 0; i < points.size(); i++) {
            TPoint p = points.get(i);
            xmin = Float.min(xmin, p.x);
            xmax = Float.max(xmax, p.x);

            ymin = Float.min(ymin, p.y);
            ymax = Float.max(ymax, p.y);

            zmin = Float.min(zmin, p.z);
            zmax = Float.max(zmax, p.z);
        }

        return new TBound(xmin, xmax, ymin, ymax, zmin, zmax);
    }

    public static BufferedWriter writerNoErr(String path) {
        try {
            return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));
        } catch (UnsupportedEncodingException ex) {

        } catch (FileNotFoundException ex) {

        }
        return null;
    }

    public static BufferedWriter writer(String path) throws FileNotFoundException, UnsupportedEncodingException {
        return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));
    }

    public static boolean hasIndex(int index, Collection<?> collection) {
        return index >= 0 && index < collection.size();
    }

    public static boolean outOfIndex(int index, Collection<?> collection) {
        return !hasIndex(index, collection);
    }

    public static <E> E element(int index, List<E> list) {
        return outOfIndex(index, list) ? null : list.get(index);
    }

    public static float makeBoxValue(float value, float sizeOfBox) {
        return value - (value % sizeOfBox);
    }

    public static double makeID(float x, float y, float z) {
        final int rangePrimitive = 1_000;
        final int rangeFloatPoint = 10_000;

        final long max = 5 * rangePrimitive * rangeFloatPoint;

        final long XX = (long) (max + x * rangeFloatPoint);
        final long YY = (long) (max + y * rangeFloatPoint);
        final long ZZ = (long) (max + z * rangeFloatPoint);
        final String sId = "" + XX + YY + ZZ;
        return Double.parseDouble(sId);
    }

    public static long addZero(long current, long max) {
        int multiple = (int) Math.pow(10, (int) (Math.log10(max) - Math.log10(current)));
        return current * multiple;
    }

    public static boolean isWithin(ArrayList<TPoint> big, ArrayList<TPoint> small) {
        Polygon bigPolygon = polygonFrom(big);
        Polygon smallPolygon = polygonFrom(small);
        for (TPoint bigPoint : big) {
            for (TPoint smallPoint : small) {
                if (bigPoint.khoangCach(smallPoint) == 0) {
                    return false;
                }
            }
        }
        return smallPolygon.within(bigPolygon);
    }

    public static void moveFirstToEnd(List<TPoint> points) {
        if (points == null || points.size() < 1) {
            return;
        }
        points.add(points.remove(0));
    }

    public static void clockwise(List<TPoint> points) {
        if (points.isEmpty()) {
            return;
        }
        TPoint leftPoint = points.get(0);
        TPoint rightPoint = points.get(0);
        TPoint topPoint = points.get(0);
        TPoint bottomPoint = points.get(0);

        for (TPoint p : points) {
            if (p.x < leftPoint.x) {
                leftPoint = p;
            }

            if (p.x > rightPoint.x) {
                rightPoint = p;
            }

            if (p.y < topPoint.y) {
                topPoint = p;
            }

            if (p.y > bottomPoint.y) {
                bottomPoint = p;
            }
        }

        int topIndex = points.indexOf(topPoint);
        int rightIndex = points.indexOf(rightPoint);
        int leftIndex = points.indexOf(leftPoint);
        int bottomIndex = points.indexOf(bottomPoint);

        int count = 0;
        count += (topIndex < rightIndex) ? 1 : 0;
        count += (rightIndex < bottomIndex) ? 1 : 0;
        count += (bottomIndex < leftIndex) ? 1 : 0;
        count += (leftIndex < topIndex) ? 1 : 0;

        if (count >= 3) {
            Collections.reverse(points);
        }
    }

    public static boolean isWithin(Polygon big, TPoint point) {
        ArrayList<TPoint> points = new ArrayList<>();
        points.add(point);
        points.add(new TPoint(point.x + 0.0001f, point.y + 0.0001f, point.z));
        points.add(new TPoint(point.x + 0.0001f, point.y - 0.0001f, point.z));
        points.add(point);

        Polygon smallPolygon = polygonFrom(points);
        return big.contains(smallPolygon);
    }

    public static boolean isWithin(ArrayList<TPoint> big, TPoint point) {
        ArrayList<TPoint> points = new ArrayList<>();
        points.add(point);
        points.add(new TPoint(point.x + 0.0001f, point.y + 0.0001f, point.z));
        points.add(new TPoint(point.x + 0.0001f, point.y - 0.0001f, point.z));
        points.add(point);
        return isWithin(big, points);
    }

    public static GeometryFactory sharedGeometryFactory = new GeometryFactory();

    public static Polygon polygon2DFrom(float... values) {
        ArrayList<TPoint> points = new ArrayList<>();
        for (int i = 0; i < values.length; i += 2) {
            float x = values[i];
            float y = values[i + 1];
            float z = 0f;
            points.add(new TPoint(x, y, z));
        }
        return polygonFrom(points);
    }

    public static Polygon polygonFrom(ArrayList<TPoint> points) {
        LinearRing ring = linearRingFrom(points);
        LinearRing[] holes = new LinearRing[0];
        return new Polygon(ring, holes, sharedGeometryFactory);
    }

    public static Polygon polygonFrom(ArrayList<TPoint> points, ArrayList<ArrayList<TPoint>> holePoints) {
        LinearRing ring = linearRingFrom(points);
        LinearRing[] holes = new LinearRing[holePoints.size()];
        for (int i = 0; i < holePoints.size(); i++) {
            holes[i] = linearRingFrom(holePoints.get(i));
        }
        return new Polygon(ring, holes, sharedGeometryFactory);
    }

    public static Coordinate[] coordinatesFrom(ArrayList<TPoint> points) {
        int size = points.size();
        Coordinate[] output = new Coordinate[size];
        for (int i = 0; i < size; i++) {
            TPoint p = points.get(i);
            output[i] = new Coordinate(p.x, p.y, p.z);
        }
        return output;
    }

    public static LinearRing linearRingFrom(ArrayList<TPoint> points) {
        Coordinate[] coordinates = coordinatesFrom(points);
        CoordinateSequence coordinateSequences = CoordinateArraySequenceFactory.instance().create(coordinates);
        LinearRing ring = new LinearRing(coordinateSequences, new GeometryFactory());
        return ring;
    }

    class DrawablePolygon {

        int borderColor;
        Polygon polygon;
    }

    public static void writePolygonsBound(String path, ArrayList<ArrayList<TPoint>> polies) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));
            for (ArrayList<TPoint> poly : polies) {
                ArrayList<TPoint> points = poly;
                for (TPoint p : points) {
                    bufferedWriter.write(String.format("(%f,%f,%f)\n", p.x, p.y, p.z));
                }
                bufferedWriter.write(String.format("(%f,%f,%f)\n", Float.NaN, Float.NaN, Float.NaN));
            }

            bufferedWriter.close();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void writeBound(String path, ArrayList<TPoint>... pointss) {
        try {
//            path = path.replace(".", "").replace("0", "");
            FileUtils.touch(new File(path));
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));
            for (ArrayList<TPoint> points : pointss) {
                for (TPoint p : points) {
                    bufferedWriter.write(String.format("(%f,%f,%f)\n", p.x, p.y, p.z));
                }
                bufferedWriter.write(String.format("(%f,%f,%f)\n", Float.NaN, Float.NaN, Float.NaN));
            }

            bufferedWriter.close();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ArrayList<TPoint> listPointOfCircle(float r, int n, float z) {
        ArrayList<TPoint> output = new ArrayList<>();
        double goc = (2 * Math.PI) / (n - 1);
        for (int i = 0; i < n; i++) {
            double g = goc * i;
            double x = Math.sin(g) * r;
            double y = Math.cos(g) * r;
            output.add(new TPoint((float) x, (float) y, z));
        }

        return output;
    }

    public static boolean is(TPoint p, int x, int y, int z) {
        return x == (int) p.x
                && y == (int) p.y
                && z == (int) p.z;
    }

    public static String removeDupSpace(String input) {
        while (input.contains("  ")) {
            input = input.replace("  ", " ");
        }
        return input;
    }

    public static String decimalString(float num) {
        Format format = new DecimalFormat("0.000E0");
        return format.format(num);
    }

    public static ArrayList<String> chunks(String source, int size) {
        ArrayList<String> output = new ArrayList<>();
        for (int start = 0; start < source.length(); start += size) {
            int end = max(start + size, source.length() - 1);
            output.add(source.substring(start, end));
        }
        return output;

    }

    public static ArrayList<TVoxel> genVoxelFromG4dcm(G4DcmReader.Data data) {
        int nx = data.nx;
        int ny = data.ny;
        int nz = data.nz;
        float xmin = data.xmin;
        float xmax = data.xmax;

        float ymin = data.ymin;
        float ymax = data.ymax;

        float zmin = data.zmin;
        float zmax = data.zmax;
        return genVoxelFromG4dcm(nx, ny, nz, xmin, xmax, ymin, ymax, zmin, zmax);
    }

    public static class BaseG4dcmInfo {

        public BaseG4dcmInfo() {
        }

        public BaseG4dcmInfo(G4DcmReader.Data data) {
            nx = data.nx;
            ny = data.ny;
            nz = data.nz;

            xmin = data.xmin;
            xmax = data.xmax;

            ymin = data.ymin;
            ymax = data.ymax;

            zmin = data.zmin;
            zmax = data.zmax;
        }

        public int nx, ny, nz;
        public float xmin, ymin, zmin;
        public float xmax, ymax, zmax;

        float dx() {
            return Math.abs(xmax - xmin) / nx;
        }

        float dy() {
            return Math.abs(ymax - ymin) / ny;
        }

        float dz() {
            return Math.abs(zmax - zmin) / nz;
        }
    }

    public static TVoxel genVoxel(BaseG4dcmInfo baseG4dcmInfo, int xi, int yi, int zi) {
        float vxmin = baseG4dcmInfo.xmin + baseG4dcmInfo.dx() * xi;
        float vymin = baseG4dcmInfo.ymin + baseG4dcmInfo.dy() * yi;
        float vzmin = baseG4dcmInfo.zmin + baseG4dcmInfo.dz() * zi;
        TVoxel voxel = new TVoxel(
                vxmin,
                vymin,
                vzmin,
                vxmin + baseG4dcmInfo.dx(),
                vymin + baseG4dcmInfo.dy(),
                vzmin + baseG4dcmInfo.dz());
        voxel.xi = xi;
        voxel.yi = yi;
        voxel.zi = zi;
        return voxel;
    }

    public static ArrayList<TVoxel> genVoxelFromG4dcm(int nx, int ny, int nz, float xmin, float xmax, float ymin, float ymax, float zmin, float zmax) {
        ArrayList<TVoxel> output = new ArrayList<>();
        float dx = Math.abs(xmax - xmin) / nx;
        float dy = Math.abs(ymax - ymin) / ny;
        float dz = Math.abs(zmax - zmin) / nz;

        for (int xi = 0; xi < nx; xi++) {
            for (int yi = 0; yi < ny; yi++) {
                for (int zi = 0; zi < nz; zi++) {
                    float vxmin = xmin + dx * xi;
                    float vymin = ymin + dy * yi;
                    float vzmin = zmin + dz * zi;
                    TVoxel voxel = new TVoxel(
                            vxmin,
                            vymin,
                            vzmin,
                            vxmin + dx,
                            vymin + dy,
                            vzmin + dz);
                    voxel.xi = xi;
                    voxel.yi = yi;
                    voxel.zi = zi;
                    output.add(voxel);
                }
            }
        }

        return output;
    }

    public static float xx(float a) {
        return a * a;
    }

    public static double xx(double a) {
        return a * a;
    }

    public static String getStringBefore(String string, String split) {
        int lastIndexOf = string.lastIndexOf(split);
        if (lastIndexOf == -1) {
            return string;
        }
        return string.substring(0, lastIndexOf);
    }

    public static Float[][][] cutFloatArray(Float[][][] array, int xi, int yi, int zi, int nx, int ny, int nz) {
        Float[][][] output = new Float[nx][ny][nz];
        for (int x = 0; x < nx; x++) {
            for (int y = 0; y < ny; y++) {
                for (int z = 0; z < nz; z++) {
                    output[x][y][z] = array[x + xi][y + yi][z + zi];
                }
            }
        }
        return output;
    }

    public static Integer[][][] cutIntArray(Integer[][][] array, int xi, int yi, int zi, int nx, int ny, int nz) {
        Integer[][][] output = new Integer[nx][ny][nz];
        for (int x = 0; x < nx; x++) {
            for (int y = 0; y < ny; y++) {
                for (int z = 0; z < nz; z++) {
                    output[x][y][z] = array[x + xi][y + yi][z + zi];
                }
            }
        }
        return output;
    }

    public static Float returnZeroIfNanInfinity(Float value) {
        if (value == null) {
            return 0f;
        }
        if (value.isNaN()) {
            return 0f;
        }
        if (value.isInfinite()) {
            return 0f;
        }
        return value;
    }

    public static int getStructIndexFromStructName(String string) {
        final String regex = "^([\\d]*)";
        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            for (int i = 1; i <= matcher.groupCount(); i++) {
                String stringGroup = matcher.group(i);
                int output = Integer.parseInt(stringGroup);
                return output;
            }
        }
        return -1;
    }

    public static float avg(ArrayList<Float> floats) {
        float sum = 0;
        for (float f : floats) {
            sum += f;
        }
        return sum / floats.size();
    }

    public static String dirOfFile(JLabel label) {
        File file = new File(label.getText());
        return file.getParent();
    }
}
