/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

import public2frame.TPoint;
import public2frame.Util;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apple
 */
public class GeomWriter {

    final float minZ, maxZ;
    final Dicom2G4Reader.Data data;
    final G4DcmReader.Data g4dcmData;
    final byte nMinZ;
    final ArrayList<String> names = new ArrayList<>();

    public GeomWriter(Dicom2G4Reader.Data data, G4DcmReader.Data g4dcmData, byte nMinZ) {
        if (g4dcmData == null) {
            ArrayList<Float> temps = new ArrayList<>();
            for (ArrayList<Dicom2G4Reader.Bound> layer : data.layers) {
                for (Dicom2G4Reader.Bound bound : layer) {
                    for (TPoint point : bound.points) {
                        temps.add(point.z);
                    }
                }
            }
            this.minZ = Collections.min(temps);;
            this.maxZ = Collections.max(temps);;
        } else {
            this.minZ = g4dcmData.zmin;
            this.maxZ = g4dcmData.zmax;
        }

        this.data = data;
        this.g4dcmData = g4dcmData;
        this.nMinZ = nMinZ;
    }

    public GeomWriter(Dicom2G4Reader.Data data, float zMin, float zMax, byte nMinZ) {

        this.minZ = zMin;
        this.maxZ = zMax;

        this.data = data;
        this.g4dcmData = null;
        this.nMinZ = nMinZ;
    }

    /**
     *
     * @param nMinZ
     */
    public GeomWriter() {
        this.minZ = Float.MIN_VALUE;
        this.maxZ = Float.MAX_VALUE;
        this.data = null;
        this.g4dcmData = null;
        nMinZ = 3;
    }

    public GeomWriter(byte nMinZ) {
        this.minZ = Float.MIN_VALUE;
        this.maxZ = Float.MAX_VALUE;
        this.data = null;
        this.g4dcmData = null;
        this.nMinZ = nMinZ;
    }

    ArrayList<Dicom2G4Reader.Bound> bounds = new ArrayList<>();

    private void startAnalyst() {
        bounds.clear();
        for (ArrayList<Dicom2G4Reader.Bound> layer : data.layers) {
            bounds.addAll(layer);
        }
    }

    public static void write(String path, ArrayList<GeomReader.Solid> solids, byte nMinZ) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));

            for (GeomReader.Solid solid : solids) {
                write(bufferedWriter, solid, nMinZ);
            }

            bufferedWriter.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void writeWithAverage(String path, ArrayList<GeomReader.Solid> solids, byte nMinZ) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));

            for (GeomReader.Solid solid : solids) {
                writeWithAverage(bufferedWriter, solid, nMinZ);
            }

            bufferedWriter.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void writeAfterFindSubstraction(String path, ArrayList<GeomReader.Solid> solids, byte nMinZ) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));

            for (GeomReader.Solid solid : solids) {
                writeAfterFindSubstraction(bufferedWriter, solid, nMinZ);
            }

            bufferedWriter.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void writeAfterFindSubstraction(BufferedWriter writer, GeomReader.Solid solid, byte nMinZ) {
        List<TPoint> subList = solid.points;
        Util.clockwise(subList);
        ArrayList<Object> infoObjects = new ArrayList<>();
        infoObjects.add(subList.size());

        for (TPoint p : subList) {
            infoObjects.add(p.x);
            infoObjects.add(p.y);
        }
        infoObjects.add(nMinZ);
        infoObjects.add(solid.z1);
        infoObjects.add(0);
        infoObjects.add(0);
        infoObjects.add(1);
        if (nMinZ == 3) {
            infoObjects.add((solid.z1 + solid.z2) / 2.0);
            infoObjects.add(0);
            infoObjects.add(0);
            infoObjects.add(1);
        }
        infoObjects.add(solid.z2);
        infoObjects.add(0);
        infoObjects.add(0);
        infoObjects.add(1);

        try {
            if (solid.typeOfSolid == GeomReader.Solid.TypeOfSolid.None) {
                final String name = solid.name;
                final String info = Util.stringFrom(infoObjects);
                final String footer = "G4_AIR";
                final String header = String.format(":VOLU %s EXTRUDED", name);
                final String allLine = header + " " + info + " " + footer + "\n";
                writer.write(allLine);
                writer.write(String.format(":PLACE %s 1 world R00 0 0 0\n", name));
            } else {
                final String name = solid.name;
                final String info = Util.stringFrom(infoObjects);
                final String header = String.format(":SOLID %s EXTRUDED", name);
                final String allLine = header + " " + info + "\n";
                writer.write(allLine);
//                if (solid.father != null && solid.father.numberOfSon == solid.indexOfSon) {
//                    final String name2 = solid.name;
//                    final String info2 = Util.stringFrom(infoObjects);
//                    final String footer2 = "G4_AIR";
//                    final String header2 = String.format(":VOLU %s EXTRUDED", name2);
//                    final String allLine2 = header2 + " " + info2 + " " + footer2 + "\n";
//                    writer.write(allLine2);
//                    writer.write(String.format(":PLACE %s 1 world R00 0 0 0\n", name2));
//                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void write(BufferedWriter writer, GeomReader.Solid solid, byte nMinZ) {
        List<TPoint> subList = solid.points;
        Util.clockwise(subList);
        ArrayList<Object> infoObjects = new ArrayList<>();
        infoObjects.add(subList.size());

        for (TPoint p : subList) {
            infoObjects.add(p.x);
            infoObjects.add(p.y);
        }
        infoObjects.add(nMinZ);
        infoObjects.add(solid.z1);
        infoObjects.add(0);
        infoObjects.add(0);
        infoObjects.add(1);
        if (nMinZ == 3) {
            infoObjects.add((solid.z1 + solid.z2) / 2.0);
            infoObjects.add(0);
            infoObjects.add(0);
            infoObjects.add(1);
        }
        infoObjects.add(solid.z2);
        infoObjects.add(0);
        infoObjects.add(0);
        infoObjects.add(1);

        final String name = solid.name;
        final String info = Util.stringFrom(infoObjects);
        final String footer = "G4_AIR";
        final String header = String.format(":VOLU %s EXTRUDED", name);
        final String allLine = header + " " + info + " " + footer + "\n";
        try {
            writer.write(allLine);
            writer.write(String.format(":PLACE %s 1 world R00 0 0 0\n", name));
        } catch (IOException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void writeWithAverage(BufferedWriter writer, GeomReader.Solid solid, byte nMinZ) {
        List<TPoint> subList = solid.points;
        Util.clockwise(subList);
        ArrayList<Object> infoObjects = new ArrayList<>();
        infoObjects.add(subList.size());

        for (TPoint p : subList) {
            infoObjects.add(p.x);
            infoObjects.add(p.y);
        }
        infoObjects.add(nMinZ);
        infoObjects.add(solid.z1);
        infoObjects.add(0);
        infoObjects.add(0);
        infoObjects.add(1);
        if (nMinZ == 3) {
            infoObjects.add((solid.z1 + solid.z2) / 2.0);
            infoObjects.add(0);
            infoObjects.add(0);
            infoObjects.add(1);
        }
        infoObjects.add(solid.z2);
        infoObjects.add(0);
        infoObjects.add(0);
        infoObjects.add(1);

        final String name = solid.name;
        final String info = Util.stringFrom(infoObjects);
        final String footer = "G4_AIR";
        final String header = String.format("%s %s EXTRUDED", solid.type, name);
        final String allLine = header + " " + info + " " + footer + "\n";
        try {
            writer.write(allLine);
            writer.write(String.format(":PLACE %s 1 world R00 0 0 0\n", name));
        } catch (IOException ex) {
            Logger.getLogger(GeomWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void write(String path, Float zWeight, byte nMinZ) {
        this.path = path;
        try {
            startAnalyst();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));
            int stt = 1;
            for (int i = 0; i < bounds.size(); i++) {
                Dicom2G4Reader.Bound bound = bounds.get(i);
                if (bound.points.size() == 0) {
                    continue;
                }
                write(bufferedWriter, bound, zWeight, nMinZ);
            }
            bufferedWriter.close();
        } catch (FileNotFoundException ex) {
            System.err.println("");
        } catch (UnsupportedEncodingException ex) {
            System.err.println("");
        } catch (IOException ex) {
            System.err.println("");
        }
    }
    private String path = "";

    private boolean write(BufferedWriter out, Dicom2G4Reader.Bound bound, float zWeight, byte nMinZ) {
        if (bound.points.size() < 4) {
            return false;
        }
        float z1 = Float.MIN_VALUE, z2 = Float.MIN_VALUE;
        for (float zi = minZ; zi <= maxZ; zi += zWeight) {
            float tempz1 = zi;
            float tempz2 = zi + zWeight;
            if (tempz1 <= bound.z && bound.z <= tempz2 && bound.z <= maxZ) {
                z1 = tempz1;
                z2 = tempz2;
                break;
            }
        }

        if (z1 == Float.MIN_VALUE || z2 == Float.MIN_VALUE) {
            System.out.println("Not In Z slice " + bound.name);
            return false;
        }
        ArrayList<Object> infoObjects = new ArrayList<>();
        List<TPoint> subList = bound.points.subList(0, bound.points.size() - 1);
        infoObjects.add(subList.size());

        for (TPoint p : subList) {
            infoObjects.add(p.x);
            infoObjects.add(p.y);
        }
        infoObjects.add(nMinZ);
        infoObjects.add(z1);
        infoObjects.add(0);
        infoObjects.add(0);
        infoObjects.add(1);
        if (nMinZ == 3) {
            infoObjects.add((z1 + z2) / 2.0);
            infoObjects.add(0);
            infoObjects.add(0);
            infoObjects.add(1);
        }
        infoObjects.add(z2);
        infoObjects.add(0);
        infoObjects.add(0);
        infoObjects.add(1);

        final String name = bound.name;
        final String info = Util.stringFrom(infoObjects);
        final String footer = "G4_AIR";
        final String header = String.format(":VOLU %s EXTRUDED", name);
        final String allLine = header + " " + info + " " + footer + "\n";
        try {
            out.write(allLine);
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    private String structName(String popularValue) {
        final String name = popularValue.toLowerCase().replace(" ", "");
        names.add(name);
        int count = 0;
        for (String n : names) {
            if (n.equals(name)) {
                count++;
            }
        }
        return name + "_" + count;
    }

}
