/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package public2frame;

import public2frame.FileHelper;
import public2frame.TColor;
import public2frame.TPoint;
import public2frame.Util;
import public2frame.TBound;
import public2frame.TVoxel;
import public2frame.FileHelper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import sun.misc.Queue;

/**
 *
 * @author apple
 */
public class G4DcmReader {

//    private final ArrayList<String> lines;
    private final String filePath;
    private final boolean needIndex, needDensity, needStructure;

    public G4DcmReader(String path) {
        this.filePath = path;
        needIndex = true;
        needDensity = true;
        needStructure = true;
    }

    public G4DcmReader(String filePath, boolean needIndex, boolean needDensity, boolean needStructure) {
        this.filePath = filePath;
        this.needIndex = needIndex;
        this.needDensity = needDensity;
        this.needStructure = needStructure;
    }

    private String cutFromFirstSpace(String text) {
        int index = text.indexOf(" ");
        return text.substring(index + 1);
    }

    public Data dataUtf8() throws UnsupportedEncodingException, IOException, FileNotFoundException, InterruptedException {
        return data("UTF-8");
    }

    public Data dataUtf16() throws UnsupportedEncodingException, IOException, FileNotFoundException, InterruptedException {
        return data("UTF-16");
    }

    public Data data() {
        try {
            return dataUtf8();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            return dataUtf16();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private void addQueueIfEmpty(Queue<String> queueString, BufferedReader breader) {
        if (!queueString.isEmpty()) {
            return;
        }
        try {
            String line = breader.readLine();
            String[] parts = line.trim().split(" ");
            for (String p : parts) {
                if (p.trim().isEmpty()) {
                    continue;
                }
                queueString.enqueue(p.trim());
            }
            if (queueString.isEmpty()) {
                addQueueIfEmpty(queueString, breader);
            }
        } catch (IOException ex) {
            System.err.println(ex.getStackTrace());
        }

    }

    public Data data(String encode) throws FileNotFoundException, UnsupportedEncodingException, IOException, InterruptedException {
        String tempString = null;
        File file = new File(filePath);
        BufferedReader breader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encode));
        Data output = new Data();
        int lineIndex = 0;

        int numberOfMaterial = Integer.parseInt(breader.readLine().trim());
        for (; lineIndex < numberOfMaterial; lineIndex++) {

            String line = breader.readLine();
            String name = cutFromFirstSpace(line);
            output.materials.add(name);
        }

        ArrayList<Integer> xyzNumbers = FileHelper.intergersInLine(breader.readLine());
        output.nx = xyzNumbers.get(0);
        output.ny = xyzNumbers.get(1);
        output.nz = xyzNumbers.get(2);

        output.indexs = new Integer[output.nx][output.ny][output.nz];
        output.densities = new Float[output.nx][output.ny][output.nz];
        output.structures = new Integer[output.nx][output.ny][output.nz];

        ArrayList<Float> minMaxX = FileHelper.floatsInLine(breader.readLine());
        output.xmin = minMaxX.get(0);
        output.xmax = minMaxX.get(1);

        ArrayList<Float> minMaxY = FileHelper.floatsInLine(breader.readLine());
        output.ymin = minMaxY.get(0);
        output.ymax = minMaxY.get(1);

        ArrayList<Float> minMaxZ = FileHelper.floatsInLine(breader.readLine());
        output.zmin = minMaxZ.get(0);
        output.zmax = minMaxZ.get(1);

        Queue<String> queueString = new Queue<>();

        output.minIndex = Integer.MAX_VALUE;
        output.maxIndex = Integer.MIN_VALUE;
        for (int zi = 0; zi < output.nz; zi++) {
            for (int yi = 0; yi < output.ny; yi++) {
                for (int xi = 0; xi < output.nx; xi++) {
                    addQueueIfEmpty(queueString, breader);
                    Integer object = Integer.parseInt(queueString.dequeue());
                    output.indexs[xi][yi][zi] = object;
                    Util.log("Read G4dcm Index %s %s %s", xi, yi, zi);
                    output.minIndex = Math.min(output.minIndex, object);
                    output.maxIndex = Math.max(output.maxIndex, object);
                }
            }
        }

        int ni = 0;
        if (needDensity) {
            output.minDensity = Float.MAX_VALUE;
            output.maxDensity = Float.MIN_VALUE;
            for (int zi = 0; zi < output.nz; zi++) {
                for (int yi = 0; yi < output.ny; yi++) {
                    for (int xi = 0; xi < output.nx; xi++) {
                        addQueueIfEmpty(queueString, breader);
                        float object = Float.parseFloat(queueString.dequeue());
                        output.densities[xi][yi][zi] = object;
                        Util.log("Read G4dcm Density %s %s %s", xi, yi, zi);
                        output.minDensity = Math.min(output.minDensity, object);
                        output.maxDensity = Math.max(output.maxDensity, object);
                    }
                }
            }
        }

        ni = 0;
        if (needStructure) {
            output.minStructure = Integer.MAX_VALUE;
            output.maxStructure = Integer.MIN_VALUE;
            for (int zi = 0; zi < output.nz; zi++) {
                for (int yi = 0; yi < output.ny; yi++) {
                    for (int xi = 0; xi < output.nx; xi++) {
                        addQueueIfEmpty(queueString, breader);
                        int object = Integer.parseInt(queueString.dequeue());
                        output.structures[xi][yi][zi] = object;
                        Util.log("Read G4dcm Structure %s %s %s", xi, yi, zi);
                        output.minStructure = Math.min(output.minStructure, object);
                        output.maxStructure = Math.max(output.maxStructure, object);
                    }
                }
            }
        }

        output.materialHash.clear();
        output.maxMaterial = 0;
        while ((tempString = breader.readLine()) != null) {
//                String line = lines.get(lineIndex++).trim();
            String line = tempString.trim();
            if (line.isEmpty()) {
                break;
            }
            System.out.println("LineIndex " + lineIndex);
            String[] parts = line.split(" ");
            Integer number = Integer.parseInt(parts[0].trim());
            String text = line.replace("\"", "").replaceFirst(parts[0] + " ", "");
            output.materialHash.put(number, text);
            output.maxMaterial = Integer.max(number, output.maxMaterial);
        }
        breader.close();
        return output;
    }

    public Data data2() {
        try {
            File file = new File(filePath);
            BufferedReader breader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
            Data output = new Data();
            int lineIndex = 0;
            int numberOfMaterial = Integer.parseInt(breader.readLine());
            for (; lineIndex < numberOfMaterial; lineIndex++) {
                String line = breader.readLine();
                String name = cutFromFirstSpace(line);
                output.materials.add(name);
            }

            ArrayList<Integer> xyzNumbers = FileHelper.intergersInLine(breader.readLine());
            output.nx = xyzNumbers.get(0);
            output.ny = xyzNumbers.get(1);
            output.nz = xyzNumbers.get(2);

            output.indexs = new Integer[output.nx][output.ny][output.nz];
            output.densities = new Float[output.nx][output.ny][output.nz];
            output.structures = new Integer[output.nx][output.ny][output.nz];

            ArrayList<Float> minMaxX = FileHelper.floatsInLine(breader.readLine());
            output.xmin = minMaxX.get(0);
            output.xmax = minMaxX.get(1);

            ArrayList<Float> minMaxY = FileHelper.floatsInLine(breader.readLine());
            output.ymin = minMaxY.get(0);
            output.ymax = minMaxY.get(1);

            ArrayList<Float> minMaxZ = FileHelper.floatsInLine(breader.readLine());
            output.zmin = minMaxZ.get(0);
            output.zmax = minMaxZ.get(1);

            output.minIndex = Integer.MAX_VALUE;
            output.maxIndex = Integer.MIN_VALUE;
            Util.Index3Creator index3Creator;
            index3Creator = new Util.Index3Creator(output.nx, output.ny, output.nz);
            while (!index3Creator.isFull()) {
                ArrayList<Integer> xs = FileHelper.intergersInLine(breader.readLine());
                for (Integer number : xs) {
                    Util.Index3 index3 = index3Creator.next();
                    output.indexs[index3.xi][index3.yi][index3.zi] = number;

                    output.minIndex = Math.min(output.minIndex, number);
                    output.maxIndex = Math.max(output.maxIndex, number);
                }
            }

            if (needDensity) {
                output.minDensity = Float.MAX_VALUE;
                output.maxDensity = Float.MIN_VALUE;
                index3Creator = new Util.Index3Creator(output.nx, output.ny, output.nz);
                while (!index3Creator.isFull()) {
                    ArrayList<Float> Floats = FileHelper.floatsInLine(breader.readLine());
                    for (Float f : Floats) {
                        Util.Index3 i3 = index3Creator.next();
                        output.densities[i3.xi][i3.yi][i3.zi] = f;
                        output.minDensity = Math.min(output.minDensity, f);
                        output.maxDensity = Math.max(output.maxDensity, f);
                    }
                }
            }

            if (needStructure) {
                output.minStructure = Integer.MAX_VALUE;
                output.maxStructure = Integer.MIN_VALUE;
                index3Creator = new Util.Index3Creator(output.nx, output.ny, output.nz);
                while (!index3Creator.isFull()) {
                    ArrayList<Integer> Integers = FileHelper.intergersInLine(breader.readLine());
                    for (Integer number : Integers) {
                        Util.Index3 i3 = index3Creator.next();
                        output.structures[i3.xi][i3.yi][i3.zi] = number;
                        output.minStructure = Math.min(output.minStructure, number);
                        output.maxStructure = Math.max(output.maxStructure, number);
                    }
                }
            }

            output.materialHash.clear();
            output.maxMaterial = 0;
            String tempString = null;
            while ((tempString = breader.readLine()) != null) {
                String line = tempString.trim();
                if (line.isEmpty()) {
                    break;
                }
                String[] parts = line.split("\"", -1);
                Integer number = Integer.parseInt(parts[0].trim());
                String text = parts[1];
                output.materialHash.put(number, text);
                output.maxMaterial = Integer.max(number, output.maxMaterial);
            }
            breader.close();
            return output;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static class Data {

        public ArrayList<String> materials = new ArrayList<>();
        public int nx, ny, nz;
        public Float xmin, xmax, ymin, ymax, zmin, zmax;
        public Integer[][][] indexs;
        public Float[][][] densities;
        public Integer[][][] structures;
        public int maxIndex, minIndex;
        public float maxDensity, minDensity;
        public int maxStructure, minStructure;
        public HashMap<Integer, String> materialHash = new HashMap<>();
        public int maxMaterial = 0;

        public float dx() {
            return Math.abs(xmax - xmin) / nx;
        }

        public float dy() {
            return Math.abs(ymax - ymin) / ny;
        }

        public float dz() {
            return Math.abs(zmax - zmin) / nz;
        }

        public TVoxel getVoxelAt(int xi, int yi, int zi) {
            float dx = Math.abs(xmin - xmax) / nx;
            float dy = Math.abs(ymin - ymax) / ny;
            float dz = Math.abs(zmin - zmax) / nz;

            float x = xmin + xi * dx;
            float y = ymin + yi * dy;
            float z = zmin + zi * dz;

            return new TVoxel(
                    x, y, z,
                    x + dx, y + dy, z + dz);
        }

        public int getStructIndex(String name) {
            String copy = name.toLowerCase().replace("\"", "");
            for (Integer key : materialHash.keySet()) {
                String value = materialHash.get(key);
                String copyValue = value.toLowerCase().replace("\"", "");
                if (copy.equals(copyValue)) {
                    return key;
                }
            }
            return -1;
        }

        public TPoint positionAt(int xi, int yi, int zi) {
            return new TPoint(
                    xmin + xi * (xmax - xmin) / nx,
                    ymin + yi * (ymax - ymin) / ny,
                    zmin + zi * (zmax - zmin) / ny);
        }

        private boolean isOutOfRange(int xi, int yi, int zi) {
            if (xi < 0 || yi < 0 || zi < 0) {
                return true;
            }

            if (xi >= nx || yi >= ny || zi >= nz) {
                return true;
            }

            return false;
        }

        public int structureOfPosition(float posX, float posY, float posZ) {
            int xi = (int) Math.floor(nx * (posX - xmin) / (xmax - xmin));
            int yi = (int) Math.floor(ny * (posY - ymin) / (ymax - ymin));
            int zi = (int) Math.floor(nz * (posZ - zmin) / (zmax - zmin));
            if (isOutOfRange(xi, yi, zi) || structures[xi][yi][zi] == null) {
                return Integer.MAX_VALUE;
            }
            return structures[xi][yi][zi];
        }

        public int indexOfPosition(float posX, float posY, float posZ) {
            int xi = (int) Math.floor(nx * (posX - xmin) / (xmax - xmin));
            int yi = (int) Math.floor(ny * (posY - ymin) / (ymax - ymin));
            int zi = (int) Math.floor(nz * (posZ - zmin) / (zmax - zmin));
            if (isOutOfRange(xi, yi, zi) || indexs[xi][yi][zi] == null) {
                return Integer.MAX_VALUE;
            }
            return indexs[xi][yi][zi];
        }

        public float densityOfPosition(float posX, float posY, float posZ) {
            int xi = (int) Math.floor(nx * (posX - xmin) / (xmax - xmin));
            int yi = (int) Math.floor(ny * (posY - ymin) / (ymax - ymin));
            int zi = (int) Math.floor(nz * (posZ - zmin) / (zmax - zmin));
            if (isOutOfRange(xi, yi, zi)|| densities[xi][yi][zi] == null) {
                return Float.MAX_VALUE;
            }
            return densities[xi][yi][zi];
        }
//
//        public ArrayList<TBound> voxelsInPolygon(ArrayList<TPoint> points) {
//            TBound bound = Util.bound(points);
//            float haftDx = (xmax - xmin) / (2 * nx);
//            float haftDy = (ymax - ymin) / (2 * ny);
//            float haftDz = (zmax - zmin) / (2 * nz);
//            ArrayList<TBound> output = new ArrayList<>();
//            int nBoundx = (int) (Math.abs(bound.xmax - bound.xmin) / haftDx);
//            int nBoundy = (int) (Math.abs(bound.ymax - bound.ymin) / haftDy);
//            int nBoundz = (int) (Math.abs(bound.zmax - bound.zmin) / haftDz);
//
//            Float maxBoundN = Util.max((float) nBoundx, (float) nBoundy, (float) nBoundz);
//            ArrayList<TPoint> polygon = new ArrayList<>(points);
//            polygon.add(points.get(0));
//            float dx = (xmax - xmin) / maxBoundN;
//            float dy = (ymax - ymin) / maxBoundN;
//            float dz = (zmax - zmin) / maxBoundN;
//            for (int xi = 0; xi < maxBoundN; xi++) {
//                for (int yi = 0; yi < maxBoundN; yi++) {
//                    TPoint p = new TPoint(xmin + xi * dx, ymin + yi * dy, points.get(0).z);
//                    if (Util.isWithin(points, p)) {
//                        TBound voxel = voxelBoundAtPoint(p.x, p.y, p.z);
//                        appendBound(output, voxel);
//                    }
//                }
//            }
//            return output;
//        }

        private void appendBound(ArrayList<TBound> all, TBound newBound) {
            for (TBound b : all) {
                if (b.equals(newBound)) {
                    return;
                }
            }
            all.add(newBound);
        }

        public TBound voxelBoundAtIndex(float xi, float yi, float zi) {
            float dx = (xmax - xmin) / nx;
            float boxMinX = xmin + xi * dx;

            float dy = (ymax - ymin) / ny;
            float boxMinY = ymin + yi * dy;

            float dz = (zmax - zmin) / nz;
            float boxMinZ = zmin + zi * dz;

            return new TBound(boxMinX, boxMinX + dx, boxMinY, boxMinY + dy, boxMinZ, boxMinZ + dz);
        }

        public TBound voxelBoundAtPoint(float posX, float posY, float posZ) {
            int xi = (int) (nx * (posX - xmin) / (xmax - xmin)) - 1;
            int yi = (int) (ny * (posY - ymin) / (ymax - ymin)) - 1;
            int zi = (int) (nz * (posZ - zmin) / (zmax - zmin)) - 1;
            return voxelBoundAtIndex(xi, yi, zi);
        }

        public int indexOfPosX(int posX) {
            return (int) (nx * (posX - xmin) / (xmax - xmin)) - 1;
        }

        public int indexOfPosY(int posY) {
            return (int) (ny * (posY - ymin) / (ymax - ymin)) - 1;
        }

        public int indexOfPosZ(int posZ) {
            return (int) (nz * (posZ - zmin) / (zmax - zmin)) - 1;
        }

        public TColor[][] getDataXY(int zi, int checkingValue) {
            TColor[][] output = new TColor[nx][ny];
            for (int xi = 0; xi < nx; xi++) {
                for (int yi = 0; yi < ny; yi++) {
                    int value = structures[xi][yi][zi];
                    output[xi][yi] = (value != checkingValue) ? TColor.white : TColor.yellow;
                }
            }

            return output;
        }

        public TColor[][] getDataXZ(int yi, int checkingValue) {
            TColor[][] output = new TColor[nx][nz];
            for (int xi = 0; xi < nx; xi++) {
                for (int zi = 0; zi < nz; zi++) {
                    int value = structures[xi][yi][zi];
                    output[xi][zi] = (value != checkingValue) ? TColor.white : TColor.yellow;
                }
            }

            return output;
        }

        public TColor[][] getDataYZ(int xi, int checkingValue) {
            TColor[][] output = new TColor[ny][nz];
            for (int yi = 0; yi < ny; yi++) {
                for (int zi = 0; zi < nz; zi++) {
                    int value = structures[xi][yi][zi];
                    output[yi][zi] = (value != checkingValue) ? TColor.white : TColor.yellow;
                }
            }

            return output;
        }
    }
}
