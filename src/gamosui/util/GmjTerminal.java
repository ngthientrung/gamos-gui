/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import org.zeroturnaround.exec.InvalidExitValueException;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.stream.LogOutputStream;

/**
 *
 * @author gamos
 */
public class GmjTerminal {

    public static class Commands {

        ArrayList<String> lines = new ArrayList<>();

        public void add1(String template) {
            lines.add(String.format(template));
        }
        public void add(String template, Object... args) {
            lines.add(String.format(template + ";", args));
        }

    }

    private String[] makeCommandlines(Commands commands) {
        ArrayList<String> list = new ArrayList<>();
        list.add("/bin/bash");
        list.add("-c");
        String singleComment = "";
        for (String l : commands.lines) {
            singleComment += l;
        }
        list.add(singleComment);
        return list.toArray(new String[list.size()]);
    }

    class MainProcess implements Runnable {

        final Commands commands;
        final JTextArea textArea;
        final Process process;

        public MainProcess(Commands commands, JTextArea textArea, Process process) {
            this.commands = commands;
            this.textArea = textArea;
            this.process = process;
        }

        @Override
        public void run() {
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    SwingUtilities.invokeLater(new PrintText(textArea, line));
                    Thread.sleep(10);
                }
            } catch (IOException ex) {
                Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    class PrintText implements Runnable {

        final JTextArea textArea;
        final String line;

        public PrintText(JTextArea textArea, String line) {
            this.textArea = textArea;
            this.line = line;
        }

        @Override
        public void run() {
            textArea.append(line + "\n");
        }

    }

    @SuppressWarnings("empty-statement")
    public void exec(Commands commands, JTextArea textArea, int iDelete) {

        if( GmjVerbose.Verb >= 2) System.out.println("COMMANDS LINES: "+commands.lines); //GDEB
        try {
            OutputStream outputStream;
            outputStream = new LogOutputStream() {
                @Override
                protected void processLine(String string) {
                    new Thread(() -> {
                        textArea.append(string + "\n");
                        int line;
                        line = textArea.getLineCount();
                        int nchars = 0;
                        if( null != textArea ) {
                            int nchars1 = textArea.getText().indexOf('\n',2);
                            int nchars2 = textArea.getText().indexOf('\r',2);
                            if( -1 == nchars2 ) {
                                nchars = nchars1;
                            } else if( nchars1 == -1 ) {
                                nchars = nchars2;
                            } else {
                                nchars = Math.min(nchars1,nchars2);
                            }
                            //System.out.println("TEXT AREA N LINESs " +line+" nchars "+nchars+" chars1 "+nchars1+" chars2 "+nchars2); //GDEB
                            if( line > iDelete ) {
                                try {
                                    textArea.getDocument().remove(0,nchars);
                                } catch (BadLocationException ex) {
                                    Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                    }).start();
                }
            };
            textArea.setText("");
            String[] cmd = makeCommandlines(commands);
            
            ProcessExecutor executor = (new ProcessExecutor())
                    .command(cmd)
                    .redirectOutput(outputStream);
  
            new Thread(() -> {
                try {
                    executor.execute();
                } catch (IOException | InterruptedException | TimeoutException | InvalidExitValueException ex) {
                    Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start();

            /*
            Thread thread = new Thread(() -> {
                try {
                    executor.execute();
                } catch (IOException | InterruptedException | TimeoutException | InvalidExitValueException ex) {
                    Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();

            while ( true ) {
                if (thread.isAlive()) {
                    System.out.println("Thread has not finished");
                } else {
                    System.out.println("Finished");
                    break;
                }
                long delayMillis = 2000; 
                try {
                    thread.join(delayMillis);
                } catch (InterruptedException ex) {
                    Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
                    break;
                }
            }
*/
//        ProcessBuilder processBuilder = new ProcessBuilder();
//        
//        processBuilder.command(cmd);
//        InputStream inputStream = processBuilder.start().getInputStream();
//        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
//
//        String line;
//        try {
//            while ((line = reader.readLine()) != null) {
//                SwingUtilities.invokeLater(new PrintText(textArea, line));
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
//        }
        } catch (InvalidExitValueException ex) {
            Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void execSilent(Commands commands) throws IOException, InterruptedException {
        Runtime rt = Runtime.getRuntime();
        String[] cmd = makeCommandlines(commands);

        Process process = rt.exec(cmd);
        int exitCode = process.waitFor();
        // System.out.println("\nExited with error code : " + exitCode);
    }
    
    public void execWait(Commands commands, JTextArea textArea, int iDelete) {

        if( GmjVerbose.Verb >= 2) System.out.println("COMMANDS LINES: "+commands.lines); //GDEB
        try {
            OutputStream outputStream;
            outputStream = new LogOutputStream() {
                @Override
                protected void processLine(String string) {
                    new Thread(() -> {
                        textArea.append(string + "\n");
                        int line;
                        line = textArea.getLineCount();
                        int nchars1 = textArea.getText().indexOf('\n',2);
                        int nchars2 = textArea.getText().indexOf('\r',2);
                        int nchars;
                        if( -1 == nchars2 ) {
                            nchars = nchars1;
                        } else if( nchars1 == -1 ) {
                            nchars = nchars2;
                        } else {
                            nchars = Math.min(nchars1,nchars2);
                        }
                        //System.out.println("TEXT AREA N LINESs " +line+" nchars "+nchars+" chars1 "+nchars1+" chars2 "+nchars2); //GDEB
                        if( line > iDelete ) {
                            try {
                                textArea.getDocument().remove(0,nchars);
                            } catch (BadLocationException ex) {
                                Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }).start();
                }
            };
            textArea.setText("");
            String[] cmd = makeCommandlines(commands);
            
            ProcessExecutor executor = (new ProcessExecutor())
                    .command(cmd)
                    .redirectOutput(outputStream);
  
            Thread thread = new Thread(() -> {
                try {
                    executor.execute();
                } catch (IOException | InterruptedException | TimeoutException | InvalidExitValueException ex) {
                    Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
            int itime = 0;
            while ( true ) {
                int delayMillis = 5000; 
                if (thread.isAlive()) {
                    int nsec = itime*delayMillis/1000;
                    int nsecp = nsec;
                    int nmin = 0;
                    if( nsec >= 60 ) {
                        nsecp = nsec%60;
                        nmin = nsec/60;
                    }
                    String msg = "Working: "+nsecp+"s, Please wait";
                    if( nmin > 0 ) msg = "Working: "+nmin+"m "+nsecp+"s, Please wait";
                    //JOptionPane.showMessageDialog(new JFrame(),msg);
                    System.out.println(msg);
                } else {
                    System.out.println("Finished");
                    break;
                }
                itime++;
                try {
                    thread.join(delayMillis);
                } catch (InterruptedException ex) {
                    Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
                    break;
                }
            }
        } catch (InvalidExitValueException ex) {
            Logger.getLogger(GmjTerminal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
