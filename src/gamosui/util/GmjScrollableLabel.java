/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui.util;

import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.Scrollable;

/**
 *
 * @author Sara
 */
public class GmjScrollableLabel extends JLabel implements Scrollable {
  public GmjScrollableLabel(ImageIcon img) {
    super(img);
  }
  public GmjScrollableLabel() {
  }

  public Dimension getPreferredScrollableViewportSize() {
    return getPreferredSize();
  }

  public int getScrollableBlockIncrement(Rectangle r, int orietation,
      int direction) {
    return 10;
  }

  public boolean getScrollableTracksViewportHeight() {
    return false;
  }

  public boolean getScrollableTracksViewportWidth() {
    return false;
  }

  public int getScrollableUnitIncrement(Rectangle r, int orientation,
      int direction) {
    return 10;
  }

}