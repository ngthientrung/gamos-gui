/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui.util;

import com.google.common.io.Resources;
import java.io.File;
import javax.swing.JLabel;

/**
 *
 * @author gamos
 */
public class GmjUtil {
    public static String previousFile = null;
    public static void showFileDialogAndSetPath(JLabel label) {
        File file = FileHelper.openFileBrowser(previousFile);
        if (file != null) {
           label.setText(file.getAbsolutePath());
           previousFile = file.getAbsolutePath();
        }
    }

    public static String previousDir = null;
    public static void showDirDialogAndSetPath(JLabel label) {
        File file = FileHelper.openDirBrowser(previousDir);
        previousDir = file.getAbsolutePath();
        if (file != null) {
            label.setText(file.getAbsolutePath());
        }
    }
    
    public static String appPath() {
        String resPath = Resources.getResource("resources/no").getPath().replace("file:", "");
        File file = new File(resPath);
        String resPathDebug = "/build/classes/resources/no";
        if(resPath.contains(resPathDebug)) {
            return resPath.replace(resPathDebug, "");
        } else {
            return resPath.replace(File.separator + "resources/no", "").replace(File.separator + "GamosUI.jar!", "");
        }
    }
    
    public static String IntegerToString( Double val ) {
        String valstr;
        if( val == Math.floor(val)) {
          valstr = String.valueOf((int)Math.round(val));
        } else {
            valstr = String.valueOf(val);
        }
        return valstr;
    }

    public static Boolean FileIsFigure( String fileName )
    {
        int ipt = fileName.lastIndexOf(".");
        if( ipt == -1 ) return false;
        String mimeType = fileName.substring(ipt+1,fileName.length());
  //      System.out.println("CHECK FILE IS FIGURE "+fileName+" "+mimeType);
        if( "gif".equals(mimeType) 
                || mimeType == "png"
                || mimeType == "eps"
                || mimeType == "ps"
                || mimeType == "pdf"
                || mimeType == "svg"
                || mimeType == "xpm"
                || mimeType == "jpg"
                || mimeType == "jpeg"
                || mimeType == "tiff" ) {
        if( GmjVerbose.Verb >= 3 ) System.out.println("TRUE     CHECK FILE IS FIGURE "+fileName+" "+mimeType);
            return true;
        }               
        
        return false;
    }

    public static String string( String str ) {
        String strOut = str;
        if( str.indexOf("\"")== 0) {
            if( str.indexOf("\"",1)== 1) {
              strOut = str.substring(1,str.length());
            }
        } else {
            strOut = "\""+str;
        }
        if( strOut.lastIndexOf("\"")== strOut.length()-1) {
            if( strOut.lastIndexOf("\"",strOut.length()-1) == strOut.length()-2) {
              strOut = strOut.substring(0,strOut.length()-1);
            }
        } else {
            strOut = strOut+"\"";
        }
        
        return strOut;
    }
}
