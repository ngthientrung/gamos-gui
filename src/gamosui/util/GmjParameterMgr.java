/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui.util;

import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class GmjParameterMgr {
    
    Map<String, Double> theParams = new TreeMap<>();
    Map<String, String> theParamStrs = new TreeMap<>();
    Map<String, List<Double> > theVParams = new TreeMap<>();
    Map<String, List<String> > theVParamStrs = new TreeMap<>();
    
    private static GmjParameterMgr theInstance = null;
    public  static GmjParameterMgr getInstance() {
        if (theInstance==null) {
            theInstance = new GmjParameterMgr();
        }
        return theInstance;
    }
    
    private GmjParameterMgr(){
        GmjFileIn fin = new GmjFileIn("GamosGUI.meta");
        if( fin.theFileReader != null ) ReadFile(fin);
    }
  
    private void ReadFile( GmjFileIn fin ){
        List<String> strVector = new ArrayList<>();
        for( ;; ) {
            strVector = fin.GetWordsInLine();
            if( strVector.size() == 0 ) break;

            if( ":P".equals(strVector.get(0)) ) {
                Double pval = null;
                if( strVector.size() < 3 ) {
                    String msg = strVector.get(0);
                    if( strVector.size() > 0 ) msg +=" " +strVector.get(1);
                    JOptionPane.showMessageDialog(new JFrame(),
                               "!!! ERROR No second word in file GAMOSGUI.meta "+msg);
                    System.exit(1);
                } else if( strVector.size() == 3 ) {
                    try { 
                        pval = Double.parseDouble(strVector.get(2));
                        theParams.put(strVector.get(1),pval);
                   //     System.out.println("READ PARAM DOUBLE "+strVector.get(1)+"="+pval); //GDEB
                    } catch (NumberFormatException nfe) {
                        JOptionPane.showMessageDialog(new JFrame(),
                                "!!! ERROR second word in file GAMOSGUI.meta must be a number, it is "+strVector.get(2)+", "+strVector.get(1)+" "+strVector.get(2));
                        System.exit(1);
                    }
                } else {
                    List<Double> pvval = new ArrayList<>();
                    for( int ii = 2; ii < strVector.size(); ii++ )  {
                        try { 
                            pval = Double.parseDouble(strVector.get(ii));
                            pvval.add(pval);
                        } catch (NumberFormatException nfe) {
                            JOptionPane.showMessageDialog(new JFrame(),
                                "!!! ERROR second word in file GAMOSGUI.meta must be a number, it is "+strVector.get(2)+", "+strVector.get(1)+" "+strVector.get(2));
                            System.exit(1);
                        }
                    }
                    theVParams.put(strVector.get(1),pvval);  
                }
            } else if( ":PS".equals(strVector.get(0)) ) {
                String val = new String();
                if( strVector.size() < 3 ) {
                    String msg = strVector.get(0);
                    if( strVector.size() > 0 ) msg +=" " +strVector.get(1);
                    JOptionPane.showMessageDialog(new JFrame(),
                               "!!! ERROR No second word in file GAMOSGUI.meta "+msg);
                    System.exit(1);
                } else if( strVector.size() == 3 ) {
                    try {
                        val = strVector.get(2);
                        if( val.length() >= 2 && 
                            val.charAt(0) =='\"' && val.charAt(val.length()-1)=='\"' ) {
                            val = val.substring(1, val.length()-1);
                        }
                        theParamStrs.put(strVector.get(1),val);
                    } catch (NumberFormatException nfe) {
                        JOptionPane.showMessageDialog(new JFrame(),
                                "!!! ERROR second word in file GAMOSGUI.meta must be a number, it is "+strVector.get(2));
                        System.exit(1);
                    }
                } else {
                    List<String> vval = new ArrayList<>();
                    for( int ii = 2; ii < strVector.size(); ii++ )  {
                        val = strVector.get(ii);
                        vval.add(val);
                    }  
                    theVParamStrs.put(strVector.get(1),vval);  
/*                if( strVector.size() > 2 && !"\"\"".equals(strVector.get(2)) ) {
                        val = strVector.get(2);
                } */
             //   System.out.println("theParamStrs.put(strVector.get(1)"+val);
                }
            }
        }
    }

    public Double GetNumericValue( String key, Double def ) {
        Double value = theParams.get(key);
        if (value != null) {
           return value;
        } else {
           SetNumericValue(key,def);
           return def;
        }
    }
    public List<Double> GetVNumericValue( String key, List<Double> def ) {
        List<Double> vvalue = theVParams.get(key);
        if( vvalue == null || vvalue.isEmpty()) {
            if( theParams.get(key) != null ) {
                vvalue = new ArrayList<>();
// System.out.println("GetVNumericValue "+key+" "+theParams.get(key)); //GDEB
                vvalue.add(theParams.get(key));
                return vvalue;
            }
            SetVNumericValue(key,def);
            return def;
        } else {
            return vvalue;
        }
    }

    public String GetStringValue( String key, String def ) {
        String value = theParamStrs.get(key);
        if (value != null && !value.isEmpty() ) {
            return value;
        } else {
            SetStringValue(key,def);
            return def;
        }
    }

    public List<String> GetVStringValue( String key, List<String> def ) {
        List<String> vvalue = theVParamStrs.get(key);
        if( vvalue == null || vvalue.isEmpty()) {
            if( theParamStrs.get(key) != null ) {
                vvalue = new ArrayList<>();
                vvalue.add(theParamStrs.get(key));
                return vvalue;
            }
            SetVStringValue(key,def);
            return def;
        } else {
            return vvalue;
        }
    }
    
    public void SetNumericValue( String key, Double value ) {
       
       theParams.put(key,value);
  //     System.out.println("SetNumericValue"+key+" = "+value);
    }

    
    public void SetVNumericValue( String key, List<Double> value ) {
       
       theVParams.put(key,value);
    }

    public void SetStringValue( String key, String value ) {
       
       theParamStrs.put(key,value);
    }
    
    public void SetVStringValue( String key, List<String> value ) {
       
       theVParamStrs.put(key,value);
    }
    
    public void SaveValues() {
     
        File file = new File("GamosGUI.meta");
        FileWriter fr = null;
        try {
            fr = new FileWriter(file);
            Iterator<Map.Entry<String, Double>> it = theParams.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, Double> pair = it.next();
                String val = gamosui.util.GmjUtil.IntegerToString(pair.getValue());
                fr.write(":P "+pair.getKey()+" "+val+"\n");
         //       System.out.println("SAVE :P "+pair.getKey()+" "+val+"\n");              
            }

            Iterator<Map.Entry<String, List<Double>>> itv = theVParams.entrySet().iterator();
            while (itv.hasNext()) {
                Map.Entry<String, List<Double>> pair = itv.next();
                if( pair.getValue().size() != 0 ) {
                    String prt = ":P "+pair.getKey();
                    for( int ii = 0; ii < pair.getValue().size(); ii++ ) {
                        String val = gamosui.util.GmjUtil.IntegerToString(pair.getValue().get(ii));
                        prt += " "+val;
                    }
                    fr.write(prt+"\n");
                }
            }

            Iterator<Map.Entry<String, String>> its = theParamStrs.entrySet().iterator();
            while (its.hasNext()) {
                Map.Entry<String, String> pair = its.next();
                String val = pair.getValue();
                if( "".equals(val) ) val = "\"\"";
                if( val.charAt(0) !='\"' || val.charAt(val.length()-1)!='\"' ) {
                    val = "\""+val+"\"";
                }
                fr.write(":PS "+pair.getKey()+" "+val+"\n");
          //      System.out.println("SAVE :PS "+pair.getKey()+" "+val+"\n");
            
            }

            Iterator<Map.Entry<String, List<String>>> itsv = theVParamStrs.entrySet().iterator();
            while (itsv.hasNext()) {
                Map.Entry<String, List<String>> pair = itsv.next();
                if( pair.getValue().size() != 0 ) {
                    String prt = ":PS "+pair.getKey();
                    for( int ii = 0; ii < pair.getValue().size(); ii++ ) {
                        String val = pair.getValue().get(ii);
                        if( val.charAt(0) !='\"' || val.charAt(val.length()-1)!='\"' ) {
                            val = "\""+val+"\"";
                        }
                        prt += " "+val;
                    }
                    fr.write(prt+"\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            //close resources
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    
    public Color GetColor( String valName, Integer valOffU, Integer valOffD, Integer valOnU, Integer valOnD ) {
        List<Double> def =  Arrays.asList(Double.valueOf(valOffU), Double.valueOf(valOffD), Double.valueOf(valOnU), Double.valueOf(valOnD));
        List<Double> temp = GetVNumericValue(valName,def);
        Color tempColor = new Color((int)Math.round(temp.get(0)),(int)Math.round(temp.get(1)),(int)Math.round(temp.get(2)),(int)Math.round(temp.get(3)));
        return tempColor;
    }
  }
     