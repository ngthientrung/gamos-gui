/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Boolean;
import java.lang.String;
import java.lang.ref.Reference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import jdk.nashorn.internal.ir.CatchNode;

/**
 */

public class GmjFileIn {

    
    public GmjFileIn( String fileName ) {
      // read input file
        try 
        {  
            File inputFile = new File(fileName);
            theFileReader = new FileReader(inputFile);   //reads the file  
            theReader = new BufferedReader(theFileReader);  //creates a buffering character input stream              
        }  
        catch(IOException e)  
        {  
            JOptionPane.showMessageDialog(null,"INPUT FILE DOES NOT EXIST "+fileName);
            theFileReader = null;
            e.printStackTrace();  
        }  
    }     
    
    public List<String> GetWordsInLine() {
        List<String> strVector = new ArrayList<>();
        String line;  
        try {
            while(true) { // loop until a non empty line is found
                if((line=theReader.readLine())!=null) {
                    strVector = SplitInWords(line);
                    return strVector;
                } else {
                    try {
                        theFileReader.close();    //closes the stream and release the resources
                    } catch (IOException ex) {
                        Logger.getLogger(GmjFileIn.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    return strVector;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e); 
        }
    
    }

    public static List<String> SplitInWords( String line ) {
        List<String> strVector = new ArrayList<>();
                    String[] words = line.split("\\s+");
                    for(String word : words) {
                        strVector.add(word);
                    }
                    
                    //--------- A pair of double quotes delimits a word, therefore, look for the
                    //          case where there is more than one word between two double quotes
                    List<String> strVector2 = new ArrayList<>();
                    String wordq = "";
                    int imerge = 0;
                    for( int jj = 0; jj < strVector.size(); jj++) {
                        String strv = strVector.get(jj);
                        if( strv.length() == 0 ) continue;
                        if( strv.charAt(0) == '\"' ) {
                            imerge = 1;
                        } 

                        if( strv.charAt(strv.length()-1) == '\"' ) {
                            if( imerge != 1 ) {
                                String err1 = " word with trailing '\"' while there is no";
                                String err2 = " previous word with leading '\"' in line ";
                                String err = err1 + err2;
                                System.out.println(err);
                            }
                            imerge = 2;
                        }
                        if( imerge == 0 ) {
                            strVector2.add( strv );
                        } else if( imerge == 1 ) {
                            if( wordq == "" ) {
                                wordq += strv.substring(1,strv.length());
                            } else {
                                wordq += strv.substring(0,strv.length());
                            }
                            wordq += " ";
                        } else if( imerge == 2 ) {
                            if( wordq == "" ) {
                                wordq += strv.substring(1,strv.length()-1);
                            }  else {
                                wordq += strv.substring(0,strv.length()-1);
                            } 
                            strVector2.add( wordq );
                            wordq = "";
                            imerge = 0;
                        }
                    }
                    if( imerge == 1 ) {
                        String err1 = " word with leading '\"' in line while there is no";
                        String err2 = " later word with trailing '\"' in line ";
                        String err = err1 + err2;
                        System.out.println(err);
                    }
                    strVector = strVector2; 
                    // String[] splitStrings = line.split("\\");
                    //   System.out.println("LINE "+line+" ZZ "+splitStrings.length); //GDEB
                    //  System.out.println("GmFileIn line: "+line+" NWORDS="+strVector.size()); //GDEB

                    return strVector;
    
    }

    public void Close() throws IOException {
        theReader.close();
    }
    
    private BufferedReader theReader;
    public FileReader theFileReader;

}
