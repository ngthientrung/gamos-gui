/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui;

import gamosui.OrganDose.GmjOrganDoseFrame;
import gamosui.frame.GamosCmdJFrame;
import gamosui.frame.MainFrame;
import gamosui.util.GmjParameterMgr;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author gamos
 */
public class GamosUI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
     // https://docs.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
     try {
           // Set cross-platform Java L&F (also called "Metal")
        UIManager.setLookAndFeel(
            UIManager.getCrossPlatformLookAndFeelClassName());
            // Set System L&F
//        UIManager.setLookAndFeel(
  //          UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException e) {
        // handle exception
        }
        catch (ClassNotFoundException e) {
        // handle exception
        } catch (InstantiationException e) {
       // handle exception
        } catch (IllegalAccessException e) {
       // handle exception
        }   
     
        int iFrame = 2;
        // System.out.println("OPENING FRAME "+iFrame); //GDEB
        if( iFrame == 1 ) {
            GamosCmdJFrame frame = new GamosCmdJFrame();
            frame.setVisible(true);
        } else if( iFrame == 2 ) {
        
            GmjOrganDoseFrame frame;
            frame = new GmjOrganDoseFrame();
        //    frame.setTitle("GAMOS Graphical User Interface");
            frame.setVisible(true);
                
            frame.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                    frame.closeWindow();
                
                } 
            }); 
        }
    }
}
