/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui.OrganDose;

import gamosui.frame.GmjButton;
import gamosui.frame.GmjResizableImagePanel;
import gamosui.util.GmjFileIn;
import gamosui.util.GmjParameterMgr;
import gamosui.util.GmjVerbose;
import gamosui.util.GmjTerminal;
import gamosui.util.GmjUtil;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.commons.io.input.ReversedLinesFileReader;

/**
 *
 * @author Sara
 */
public class GmjOrganDoseFrame extends javax.swing.JFrame {

    /**
     * Creates new form GmjOrganDoseFrame
     */
    public GmjOrganDoseFrame() {
        try {
            new GmjVerbose();
            setExtendedState(JFrame.MAXIMIZED_BOTH);
            BufferedImage bufferedImage = ImageIO.read(new File("src/resources/marble.png"));
            Image image = bufferedImage.getScaledInstance(1600, 1000, Image.SCALE_SMOOTH);
            JLabel background = new JLabel(new ImageIcon(image));
            setContentPane(background);
            background.setLayout(new FlowLayout());

            Init();

            JPanel[] opacityPanels = new JPanel[]{
                jPanel3, jPanel5, jPanel7, jPanel6
            };

            for (JPanel p : opacityPanels) {
                p.setOpaque(false);
            }
        } catch (IOException ex) {
            Logger.getLogger(GmjOrganDoseFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String fixPath(String path) {
        String dupSep = File.separator + File.separator;
        while (path.contains(dupSep)) {
            path = path.replace(dupSep, File.separator);
        }
        return path;
    }

    private void Init() {

        //--- INIT VARIABLES
        theSourceStList = new ArrayList<>();
        theParamMgr = GmjParameterMgr.getInstance();
        theStList = new ArrayList<>();
        theIsotopesList = new ArrayList<>();
        theRTSTRUCTgeomFile = "";
        theExecuteLineDelete = 30;

        //--- SET PANEL 
//        int width = theParamMgr.GetNumericValue("WIDTH", 1000.).intValue(); 
//        int height = theParamMgr.GetNumericValue("HEIGHT", 800.).intValue();
//        thePanel = new GmjResizableImagePanel("marble.png",width,height);
//        setContentPane(thePanel);
        initComponents();

        //---- RESIZE PANEL
//        this.setSize(new Dimension(width,height));
        //--- FRAME TITLE
        setFont(new Font("System", Font.PLAIN, 14));
        Font f = getFont();
        FontMetrics fm = getFontMetrics(f);
        String GGUI = "GAMOS Organ Dose Graphical User Interface";
        int x = fm.stringWidth(GGUI);
        int y = fm.stringWidth(" ");
        int z = getWidth() / 2 - (x / 2);
        int w = z / y;
        String pad = "";
        pad = String.format("%" + w + "s", pad);
        setTitle(pad + GGUI);

        //---- SET GAMOS PATH
        theGamosPath = theParamMgr.GetStringValue("GAMOS_BASE", "");
        if (!"".equals(theGamosPath)) {
            if (Character.toString(theGamosPath.charAt(theGamosPath.length() - 1)) != File.separator) {
                theGamosPath += File.separator;
            }
            File filegp = new File(theGamosPath);
            if (!filegp.isDirectory()) {
                JOptionPane.showMessageDialog(new JFrame(),
                        "!WARNING: GAMOS_BASE DIRECTORY " + theGamosPath + " DOES NOT EXIST");
                theGamosPath = "";
            } else {
                gamosPathLabel.setText(theGamosPath);
                theGamosConfFile = theGamosPath + File.separator + "config" + File.separator + "confgamos.sh";
            }
        }

        //---- SET RUNNING DIR
        theRunningDir = theParamMgr.GetStringValue("RUNNING_DIR", "");
        if (!"".equals(theRunningDir)) {
            if (Character.toString(theRunningDir.charAt(theRunningDir.length() - 1)) != File.separator) {
                theRunningDir += File.separator;
            }
            File filerd = new File(fixPath(theRunningDir));
            if (!filerd.isDirectory()) {
                JOptionPane.showMessageDialog(new JFrame(),
                        "!WARNING: RUNNING_DIR DIRECTORY " + theRunningDir + " DOES NOT EXIST");
                theRunningDir = "";
            } else {
                runningDirLabel.setText(theRunningDir);
            }
        }

        //---- ENABLE BUTTONS IF GAMOS_PATH & RUNNING_DIR ARE DEFINED
        if (CheckGamosIsSet("", true)) {
            enableButtons(true);
        } else {
            enableButtons(false);
        }
        //   rbuttonSelectSt.setEnabled(true); //GDEB-
        if (GmjVerbose.Verb >= 3) {
            System.out.println("GAMOS PATH= " + theGamosPath + " RUNNING_DIR= " + theRunningDir);
        }

        //---- CHECK IF CT FILE IS DEFINED
        theParamMgr = GmjParameterMgr.getInstance();
        theG4dcmCTFile = theParamMgr.GetStringValue("GAMOS_CT_FILE", "");
        //---- READ GAMOS g4dcm FILE
        if (!"".equals(theG4dcmCTFile)) {
            readG4dcmCT(theG4dcmCTFile); // build list of structures it they exists
        }

        //---- CHECK IF RTSTRUCT FILE IS DEFINED
        bCalculateInStruct = false;
        theRTSTRUCTFile = theParamMgr.GetStringValue("DICOM_RTSTRUCT_FILE", "");
        if (!"".equals(theRTSTRUCTFile)) {
            if (CheckGamosIsSet(theRTSTRUCTFile, true)) {
                Boolean isOK = readRTSTRUCT(theRTSTRUCTFile); // read file to fill list of structures
                if (isOK) {
                    labelRTSTRUCT.setText(theRTSTRUCTFile);
                }
            }
        }
        if (!"".equals(theRTSTRUCTFile)) {
            struct2G4volsButton.setEnabled(true);
        }

        //---- CHECK IF NM FILE IS DEFINED
        theG4dcmNMFile = theParamMgr.GetStringValue("GAMOS_NM_FILE", "");
        if (!"".equals(theG4dcmNMFile)) {
            File filet = new File(theG4dcmNMFile);
            if (!filet.exists()) {
                JOptionPane.showMessageDialog(new JFrame(),
                        "!WARNING: GAMOS NM FILE " + theG4dcmNMFile + " DOES NOT EXIST");
                theG4dcmNMFile = "";
            } else {
                labelNM.setText(theG4dcmNMFile);
                // bNMFromDICOM = false;
            }
        }

        //--- SET NUMBER OF EVENTS
        theNEvents = (int) Math.round(theParamMgr.GetNumericValue("NEVENTS", 10000.));
        NEVText.setText(String.valueOf(theNEvents));

        //--- CHECK ISOTOPES
        List<String> isots = new ArrayList<>();
        isots = theParamMgr.GetVStringValue("ISOTOPES", isots);
        for (int i1 = 0; i1 < isots.size(); ++i1) {
            Boolean bFound = false;
            for (int i2 = 0; i2 < i1 - 1; ++i2) {
                if (isots.get(i1).equals(isots.get(i2))) {
                    bFound = true;
                    break;
                }
            }
            if (!bFound) {
                theIsotopesList.add(isots.get(i1));
            }
        }
        if (!theIsotopesList.isEmpty()) {
            String isotlist = "";
            isotlist = theIsotopesList.stream().map((isot) -> isot + " ").reduce(isotlist, String::concat);
            isotopesLabel.setText(isotlist);
            isotopesLabel.setOpaque(true);
            isotopesLabel.setVisible(true);
            isotopesLabel.setEnabled(true);
        }

        //--- POST INITIALIZE VARIABLES
        outputDosesButton.setText("<html><font size=+0><center>&nbsp;&nbsp;OUTPUT&nbsp;&nbsp;"
                + "<center>&nbsp;&nbsp;Organ Doses&nbsp;&nbsp;</center></font></html>");
        outputFiguresButton.setText("<html><font size=+0><center>&nbsp;&nbsp;OUTPUT&nbsp;&nbsp;"
                + "<center>    Figures   </center></font></html>");
        struct2G4volsButton.setText("<html><font size=+0><center>RTSTRUCT polygones</center>"
                + "<center>to Geant4 Volumes</center></font></html>");
        structGeomButton.setText("<html><font size=+0><center>Check Geant4</center>"
                + "<center>volumes</center></font></html>");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
//    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        buttonGroup6 = new javax.swing.ButtonGroup();
        buttonGroup7 = new javax.swing.ButtonGroup();
        buttonGroup8 = new javax.swing.ButtonGroup();
        buttonGroup9 = new javax.swing.ButtonGroup();
        buttonGroup10 = new javax.swing.ButtonGroup();
        buttonGroup11 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new GmjButton(6);
        jPanel5 = new javax.swing.JPanel();
        runningDirButton = new GmjButton(4);
        runningDirLabel = new javax.swing.JLabel();
        gamosPathLabel = new javax.swing.JLabel();
        gamosbaseButton = new GmjButton(4);
        jPanel6 = new javax.swing.JPanel();
        executeButton = new GmjButton(2);
        jScrollPane1 = new javax.swing.JScrollPane();
        outputTextArea = new javax.swing.JTextArea();
        outputDosesButton = new GmjButton(3);
        executeLabel = new javax.swing.JLabel();
        outputFiguresButton = new GmjButton(3);
        dosePanel = new javax.swing.JPanel();
        labelPatient2 = new javax.swing.JLabel();
        effectDoseCheckBox = new javax.swing.JCheckBox();
        doseDepoCheckBox = new javax.swing.JCheckBox();
        jPanel7 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        RTSTRUCTButton = new GmjButton(1);
        labelRTSTRUCT = new javax.swing.JLabel();
        labelPatient1 = new javax.swing.JLabel();
        calcInStCheckBox = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        labelCT = new javax.swing.JLabel();
        CTButton = new GmjButton(1);
        CTg4dcmButton = new GmjButton(1);
        labelPatient = new javax.swing.JLabel();
        structGeomButton = new GmjButton(3);
        NEVText = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        NMButton = new GmjButton(1);
        NMg4dcmButton = new GmjButton(1);
        NMImageRbutton = new javax.swing.JRadioButton();
        AllStRbutton = new javax.swing.JRadioButton();
        SelectStRbutton = new javax.swing.JRadioButton();
        labelSource = new javax.swing.JLabel();
        labelNM = new javax.swing.JLabel();
        stSelectedLabel = new javax.swing.JLabel();
        labelCTSource = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        labelCTSource2 = new javax.swing.JLabel();
        CTImageRButton = new javax.swing.JRadioButton();
        jPanel9 = new javax.swing.JPanel();
        isotopesLabel = new javax.swing.JLabel();
        isotopesButton = new GmjButton(1);
        labelNEV = new javax.swing.JLabel();
        struct2G4volsButton = new GmjButton(5);

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setPreferredSize(new java.awt.Dimension(0, 1));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 752, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setText("HELP");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        runningDirButton.setText("RUNNING_DIR");
        runningDirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runningDirButtonActionPerformed(evt);
            }
        });

        runningDirLabel.setBackground(new java.awt.Color(255, 255, 255));
        runningDirLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        runningDirLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        runningDirLabel.setOpaque(true);

        gamosPathLabel.setBackground(new java.awt.Color(255, 255, 255));
        gamosPathLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        gamosPathLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        gamosPathLabel.setOpaque(true);

        gamosbaseButton.setText("GAMOS_BASE");
        gamosbaseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gamosbaseButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(gamosbaseButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(runningDirButton, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(gamosPathLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 1631, Short.MAX_VALUE)
                    .addComponent(runningDirLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(gamosPathLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(gamosbaseButton, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(runningDirLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(runningDirButton, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );

        executeButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        executeButton.setForeground(new java.awt.Color(255, 255, 255));
        executeButton.setText("Execute");
        executeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                executeButtonActionPerformed(evt);
            }
        });

        outputTextArea.setBackground(java.awt.SystemColor.activeCaptionText);
        outputTextArea.setColumns(20);
        outputTextArea.setFont(new java.awt.Font("Ubuntu", 0, 12)); // NOI18N
        outputTextArea.setForeground(java.awt.Color.orange);
        outputTextArea.setLineWrap(true);
        outputTextArea.setRows(5);
        jScrollPane1.setViewportView(outputTextArea);

        outputDosesButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        outputDosesButton.setForeground(new java.awt.Color(0, 0, 102));
        outputDosesButton.setText("OUTPUT Doses per organ");
        outputDosesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputDosesButtonActionPerformed(evt);
            }
        });

        executeLabel.setBackground(new java.awt.Color(255, 255, 255));
        executeLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        executeLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        executeLabel.setOpaque(true);
        executeLabel.setPreferredSize(new java.awt.Dimension(34, 23));

        outputFiguresButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        outputFiguresButton.setForeground(new java.awt.Color(0, 0, 102));
        outputFiguresButton.setText("OUTPUT Figures");
        outputFiguresButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputFiguresButtonActionPerformed(evt);
            }
        });

        dosePanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102), 4), javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 4)));
        dosePanel.setOpaque(false);

        labelPatient2.setBackground(new java.awt.Color(0, 0, 0));
        labelPatient2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelPatient2.setForeground(new java.awt.Color(255, 255, 255));
        labelPatient2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelPatient2.setText("Dose Output:");
        labelPatient2.setOpaque(true);

        effectDoseCheckBox.setBackground(new java.awt.Color(255, 231, 164));
        effectDoseCheckBox.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        effectDoseCheckBox.setText(" Effective dose");
        effectDoseCheckBox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        effectDoseCheckBox.setOpaque(false);
        effectDoseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effectDoseCheckBoxActionPerformed(evt);
            }
        });

        doseDepoCheckBox.setBackground(new java.awt.Color(255, 231, 164));
        doseDepoCheckBox.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        doseDepoCheckBox.setSelected(true);
        doseDepoCheckBox.setText(" Dose deposited");
        doseDepoCheckBox.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        doseDepoCheckBox.setEnabled(false);
        doseDepoCheckBox.setOpaque(false);
        doseDepoCheckBox.setPreferredSize(new java.awt.Dimension(129, 23));
        doseDepoCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doseDepoCheckBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dosePanelLayout = new javax.swing.GroupLayout(dosePanel);
        dosePanel.setLayout(dosePanelLayout);
        dosePanelLayout.setHorizontalGroup(
            dosePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dosePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelPatient2, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(dosePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(effectDoseCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(doseDepoCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        dosePanelLayout.setVerticalGroup(
            dosePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dosePanelLayout.createSequentialGroup()
                .addGroup(dosePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dosePanelLayout.createSequentialGroup()
                        .addComponent(labelPatient2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dosePanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(doseDepoCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(effectDoseCheckBox)))
                .addContainerGap())
        );

        dosePanelLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {doseDepoCheckBox, effectDoseCheckBox});

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(executeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(executeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(dosePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(outputFiguresButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(outputDosesButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jScrollPane1)))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(outputDosesButton, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(outputFiguresButton, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(dosePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(executeButton, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                    .addComponent(executeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(5, 5, 5))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {outputDosesButton, outputFiguresButton});

        jPanel2.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102), 4), javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 4)));
        jPanel2.setOpaque(false);

        RTSTRUCTButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        RTSTRUCTButton.setText("DICOM RTSTRUCT");
        RTSTRUCTButton.setEnabled(false);
        RTSTRUCTButton.setFocusable(false);
        RTSTRUCTButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        RTSTRUCTButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        RTSTRUCTButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RTSTRUCTButtonActionPerformed(evt);
            }
        });

        labelRTSTRUCT.setBackground(new java.awt.Color(255, 255, 255));
        labelRTSTRUCT.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelRTSTRUCT.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        labelRTSTRUCT.setOpaque(true);

        labelPatient1.setBackground(new java.awt.Color(0, 0, 0));
        labelPatient1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelPatient1.setForeground(new java.awt.Color(255, 255, 255));
        labelPatient1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelPatient1.setText("Organs / TVs:");
        labelPatient1.setOpaque(true);

        calcInStCheckBox.setBackground(new java.awt.Color(105, 230, 145));
        calcInStCheckBox.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        calcInStCheckBox.setText(" Calculate in Structures Only");
        calcInStCheckBox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        calcInStCheckBox.setOpaque(false);
        calcInStCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calcInStCheckBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(calcInStCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(RTSTRUCTButton, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelRTSTRUCT, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(labelPatient1, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(labelPatient1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(labelRTSTRUCT, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(RTSTRUCTButton, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(calcInStCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102), 4), javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 4)));
        jPanel1.setEnabled(false);
        jPanel1.setOpaque(false);

        labelCT.setBackground(new java.awt.Color(255, 255, 255));
        labelCT.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelCT.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        labelCT.setOpaque(true);

        CTButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        CTButton.setForeground(new java.awt.Color(255, 255, 255));
        CTButton.setText("DICOM CT");
        CTButton.setEnabled(false);
        CTButton.setFocusable(false);
        CTButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        CTButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        CTButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CTButtonActionPerformed(evt);
            }
        });

        CTg4dcmButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        CTg4dcmButton.setText("GAMOS CT");
        CTg4dcmButton.setEnabled(false);
        CTg4dcmButton.setFocusable(false);
        CTg4dcmButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        CTg4dcmButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        CTg4dcmButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CTg4dcmButtonActionPerformed(evt);
            }
        });

        labelPatient.setBackground(new java.awt.Color(0, 0, 0));
        labelPatient.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelPatient.setForeground(new java.awt.Color(255, 255, 255));
        labelPatient.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelPatient.setText("Patient:");
        labelPatient.setOpaque(true);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(CTButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(CTg4dcmButton, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelCT, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(labelPatient, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(labelPatient, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(CTButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CTg4dcmButton, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(53, Short.MAX_VALUE)
                .addComponent(labelCT, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );

        structGeomButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        structGeomButton.setText("Structure geometry");
        structGeomButton.setEnabled(false);
        structGeomButton.setPreferredSize(new java.awt.Dimension(155, 27));

        NEVText.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        NEVText.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        NEVText.setText("1000");
        NEVText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                NEVTextFocusLost(evt);
            }
        });
        NEVText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NEVTextActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102), 4), javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 4)));
        jPanel4.setOpaque(false);

        NMButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        NMButton.setText("DICOM PET/SPECT");
        NMButton.setEnabled(false);
        NMButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NMButtonActionPerformed(evt);
            }
        });

        NMg4dcmButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        NMg4dcmButton.setText("GAMOS PET/SPECT");
        NMg4dcmButton.setEnabled(false);
        NMg4dcmButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NMg4dcmButtonActionPerformed(evt);
            }
        });

        buttonGroup1.add(NMImageRbutton);
        NMImageRbutton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        NMImageRbutton.setText("From PET/SPECT image");
        NMImageRbutton.setEnabled(false);
        NMImageRbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NMImageRbuttonActionPerformed(evt);
            }
        });

        buttonGroup1.add(AllStRbutton);
        AllStRbutton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        AllStRbutton.setText("In All structures");
        AllStRbutton.setEnabled(false);
        AllStRbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AllStRbuttonActionPerformed(evt);
            }
        });

        buttonGroup1.add(SelectStRbutton);
        SelectStRbutton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SelectStRbutton.setText("In selected structures");
        SelectStRbutton.setEnabled(false);
        SelectStRbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SelectStRbuttonActionPerformed(evt);
            }
        });

        labelSource.setBackground(new java.awt.Color(0, 0, 0));
        labelSource.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelSource.setForeground(new java.awt.Color(255, 255, 255));
        labelSource.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelSource.setText("Particle source:");
        labelSource.setOpaque(true);

        labelNM.setBackground(new java.awt.Color(255, 255, 255));
        labelNM.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelNM.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        labelNM.setOpaque(true);

        stSelectedLabel.setBackground(new java.awt.Color(255, 255, 255));
        stSelectedLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        stSelectedLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        stSelectedLabel.setOpaque(true);
        stSelectedLabel.setPreferredSize(new java.awt.Dimension(34, 23));
        stSelectedLabel.setVerifyInputWhenFocusTarget(false);

        labelCTSource.setBackground(new java.awt.Color(255, 255, 255));
        labelCTSource.setFocusable(false);
        labelCTSource.setOpaque(true);

        labelCTSource2.setBackground(new java.awt.Color(255, 255, 255));
        labelCTSource2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelCTSource2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        labelCTSource2.setOpaque(true);

        buttonGroup1.add(CTImageRButton);
        CTImageRButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        CTImageRButton.setText("In CT Voxels");
        CTImageRButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CTImageRButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CTImageRButton, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelCTSource2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CTImageRButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCTSource2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5))
        );

        isotopesLabel.setBackground(new java.awt.Color(255, 255, 255));
        isotopesLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        isotopesLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        isotopesLabel.setEnabled(false);
        isotopesLabel.setOpaque(true);
        isotopesLabel.setPreferredSize(new java.awt.Dimension(44, 23));

        isotopesButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        isotopesButton.setText("ISOTOPE(S)");
        isotopesButton.setPreferredSize(new java.awt.Dimension(145, 43));
        isotopesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isotopesButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(isotopesButton, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(isotopesLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(isotopesButton, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(isotopesLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(0, 0, 0)
                        .addComponent(labelCTSource))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(SelectStRbutton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(AllStRbutton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(stSelectedLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(NMImageRbutton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(NMg4dcmButton, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                            .addComponent(NMButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelNM, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(labelSource, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(labelSource, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(labelCTSource, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(NMButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(NMg4dcmButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(NMImageRbutton))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(labelNM, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(AllStRbutton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(SelectStRbutton))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(stSelectedLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10))
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {labelCTSource, labelNM});

        labelNEV.setBackground(new java.awt.Color(105, 230, 145));
        labelNEV.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelNEV.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelNEV.setText("Number of events=");
        labelNEV.setOpaque(true);

        struct2G4volsButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        struct2G4volsButton.setForeground(new java.awt.Color(255, 255, 255));
        struct2G4volsButton.setText("RTSTRUCT to G4 vol.");
        struct2G4volsButton.setEnabled(false);
        struct2G4volsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                struct2G4volsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(struct2G4volsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(structGeomButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 771, Short.MAX_VALUE)
                        .addComponent(labelNEV, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(NEVText, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(structGeomButton, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(struct2G4volsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(NEVText, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelNEV, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 752, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 1135, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CTButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CTButtonActionPerformed
        // TODO add your handling code here:
        GmjUtil.showDirDialogAndSetPath(labelCT);
        theCTDir = labelCT.getText();
        theParamMgr.SetStringValue("DICOM_CT_DIR", theCTDir);
        theParamMgr.SetStringValue("GAMOS_CT_FILE", "");
        // System.out.println("bCTFromDICOM but "+bCTFromDICOM); //GDEB

        try (Stream<Path> walk = Files.walk(Paths.get(labelCT.getText()))) {
            theCTFiles = walk.filter(Files::isRegularFile).map(x -> x.toString()).collect(Collectors.toList());
            theCTFiles.forEach(System.out::println);
            System.out.println("CTFILES" + theCTFiles + "\n"); //GDEB

        } catch (IOException e) {
            e.printStackTrace();
        }

        //----- CONVERT DICOM CT to GAMOS g4dcm
        if (!"".equals(theRTSTRUCTFile)) {
            runCTDICOM2G4();
        }

    }//GEN-LAST:event_CTButtonActionPerformed

    private void NMImageRbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NMImageRbuttonActionPerformed
        // TODO add your handling code here:
        NMButton.setEnabled(true);
        NMg4dcmButton.setEnabled(true);
        labelNM.setText(theG4dcmNMFile);
        labelCTSource2.setText("");
        stSelectedLabel.setText("");
        theSourceStList.clear();

        theParamMgr.SetStringValue("GAMOS_NM_FILE", theG4dcmNMFile);

    }//GEN-LAST:event_NMImageRbuttonActionPerformed

    private void AllStRbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AllStRbuttonActionPerformed
        // TODO add your handling code here:
        NMButton.setEnabled(false);
        NMg4dcmButton.setEnabled(false);

        theSourceStList = theStList;
//        System.out.println("theSourceStList "+ theSourceStList); //GDEB
        stSelectedLabel.setVisible(true);
        String stlist = "";
        for (String st : theSourceStList) {
            stlist += GmjUtil.string(st) + " ";
        }
        stSelectedLabel.setText(stlist);
        stSelectedLabel.setOpaque(true);
        stSelectedLabel.setVisible(true);
        cleanNMLabel();
        labelCTSource2.setText("");

        struct2G4volsButton.setEnabled(true);
        effectDoseCheckBox.setEnabled(true);
    }//GEN-LAST:event_AllStRbuttonActionPerformed

    private void SelectStRbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SelectStRbuttonActionPerformed
        // TODO add your handling code here:
        NMButton.setEnabled(false);
        NMg4dcmButton.setEnabled(false);

        int nRows = theStList.size();
        int maxchlen = 0;
        for (int ir = 0; ir < nRows; ++ir) {
            maxchlen = Math.max(maxchlen, theStList.get(ir).length());
        }
        Math.min(50, maxchlen); // max dialogWith=90*10+40(borders)+60(2nd column width)
        int stNameWidth = maxchlen * 8;

        int dialogWidth = Math.max(197 * 2 + 30, stNameWidth * 2 + 60); // 197=design width
        int tableWidth = Math.min(500 - 40, stNameWidth + 10);

        int dialogHeight = Math.min(700, (nRows) * 18 + 150);
        dialogHeight = Math.max(297, dialogHeight); // 297=design height
        int tableHeight = Math.min(630 - 70, (nRows) * 18);
        tableHeight = Math.max(38, tableHeight);

        GmjODSelectStDialog selectStD;
        selectStD = new GmjODSelectStDialog(this, true,
                 dialogWidth, dialogHeight, tableWidth, tableHeight);
        selectStD.Init(theStList);
        selectStD.setResizable(false);
        selectStD.setVisible(true);

        if (selectStD.GetStListSelected().size() != 0) {
            theSourceStList = selectStD.GetStListSelected();
            //          System.out.println("theSourceStList "+ theSourceStList); //GDEB
            stSelectedLabel.setVisible(true);
            String stlist = "";
            for (String st : theSourceStList) {
                stlist += GmjUtil.string(st) + " ";
            }
            stSelectedLabel.setText(stlist);
            stSelectedLabel.setOpaque(true);
            stSelectedLabel.setVisible(true);
            cleanNMLabel();
            labelCTSource2.setText("");
            struct2G4volsButton.setEnabled(true);
            effectDoseCheckBox.setEnabled(true);
        }
    }//GEN-LAST:event_SelectStRbuttonActionPerformed

    private void CTg4dcmButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CTg4dcmButtonActionPerformed
        // TODO add your handling code here:
        GmjUtil.showFileDialogAndSetPath(labelCT);
        Boolean isOK = false;
        if (!"".equals(labelCT.getText())) {
            readG4dcmCT(labelCT.getText());
        }

    }

    public void dummy() {
    }//GEN-LAST:event_CTg4dcmButtonActionPerformed


    private void struct2G4volsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_struct2G4volsButtonActionPerformed
        // TODO add your handling code here:
        GmjODRTSt2G4Dialog dialog = new GmjODRTSt2G4Dialog(this, true);
        dialog.ctDatFileName = dicomCTmdat;
        dialog.theRunningDir = theRunningDir;
        dialog.theGamosConfFile = theGamosConfFile;
        dialog.setResizable(false);
        dialog.setVisible(true);
        // !! RUN TOOL AND PRODUCE theRTSTRUCTgeomFile
    }//GEN-LAST:event_struct2G4volsButtonActionPerformed

    private void gamosbaseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gamosbaseButtonActionPerformed
        // TODO add your handling code here:
        GmjUtil.showDirDialogAndSetPath(gamosPathLabel);
        theParamMgr.SetStringValue("GAMOS_BASE", gamosPathLabel.getText());
        theGamosPath = gamosPathLabel.getText() + File.separator;
        theGamosConfFile = theGamosPath + File.separator + "config" + File.separator + "confgamos.sh";
        if (CheckGamosIsSet("", true)) {
            enableButtons(true);
        } else {
            enableButtons(false);
        }
    }//GEN-LAST:event_gamosbaseButtonActionPerformed


    private void executeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_executeButtonActionPerformed
        // TODO add your handling code here:
        if (theIsotopesList.size() == 0) {
            JOptionPane.showMessageDialog(new JFrame(),
                    "!WARNING: SELECT ISOTOPES FIRST");
            return;
        }

        // List<String> fileNames = new ArrayList<>();
        //----- RUN GAMOS INPUT FILES
        for (int ii = 0; ii < theIsotopesList.size(); ++ii) {
            String isotope = theIsotopesList.get(ii);
            String fileName = buildGAMOSInput(isotope);

            if (GmjVerbose.Verb >= 2) {
                System.out.println("EXECUTE " + "gamos " + fileName);
            }
            GmjTerminal terminal = new GmjTerminal();
            GmjTerminal.Commands commands;
            commands = new GmjTerminal.Commands();
            commands.add("mkdir -p " + theRunningDir);
            commands.add("cd " + theRunningDir);
            commands.add("source " + theGamosConfFile);
            commands.add("gamos " + fileName);

            String sqdIsot = "sqdose.outod" + isotope;
            String msg = "drawDICOM -fG4dcmCT " + File.separator + theG4dcmCTFile + " -palette 55 -rotateXY 180.";
            if (!"".equals(theRTSTRUCTFile)) {
                msg += "-fRTStruct " + theRTSTRUCTFile;
            }
            commands.add(msg);
            if (!"".equals(labelNM.getText())) {
                commands.add("drawDICOM -fG4dcmNM " + labelNM.getText() + " -palette 55 -rotateXY 180.");
            }
            commands.add(theGamosPath + File.separator + "gui" + File.separator + "dicomImagesReorder.py");
            commands.add("mkdir -p out" + isotope + File.separator + "figs." + isotope);
            commands.add("mv hisG4dcmCT*gif out" + isotope + File.separator + "figs." + isotope);
            commands.add(" mv " + sqdIsot + " out" + isotope + "; cd out" + isotope
                    + "; drawDICOM -fSqdose " + sqdIsot + " -palette 55 -rotateXY 180.");
            commands.add(theGamosPath + File.separator + "gui" + File.separator + "dicomImagesReorder.py");
            commands.add("mv hisSqdose*gif out" + isotope + File.separator + "figs." + isotope);
            commands.add("getOrganInfo -fSqdose " + sqdIsot + " -fG4dcmCT .." + File.separator + theG4dcmCTFile + " -fOut organDose." + isotope + ".out");
            commands.add(theGamosPath + File.separator + "gui" + File.separator + "dicomImagesReorder.py");
            commands.add("mkdir organDose." + isotope + "; "
                    + " mv *getOrganDose*gif organDose." + isotope + "; mv organDose." + isotope + ".out organDose." + isotope);

            terminal.exec(commands, outputTextArea, theExecuteLineDelete);
        }

        outputDosesButton.setEnabled(true);

    }//GEN-LAST:event_executeButtonActionPerformed

    private void NMButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NMButtonActionPerformed
        // TODO add your handling code here:
        GmjUtil.showFileDialogAndSetPath(labelNM);
        theNMFile = labelNM.getText();
        theParamMgr.SetStringValue("DICOM_NM_FILE", theNMFile);
        theParamMgr.SetStringValue("GAMOS_NM_FILE", "");

        //      bNMFromDICOM = true;
        // theParamMgr.SetNumericValue("bNMFromDICOM",bNMFromDICOM?1.0:0.0);
        if (!"".equals(labelNM.getText())) {
            stSelectedLabel.setText("");
            theSourceStList.clear();
            try {
                runNMDICOM2G4();
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(GmjOrganDoseFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_NMButtonActionPerformed

    private void NMg4dcmButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NMg4dcmButtonActionPerformed
        // TODO add your handling code here
        GmjUtil.showFileDialogAndSetPath(labelNM);
        //     bNMFromDICOM = false; 
        //   theParamMgr.SetNumericValue("bNMFromDICOM",bNMFromDICOM?1.0:0.0);
        theParamMgr.SetStringValue("GAMOS_NM_FILE", labelNM.getText());
        theParamMgr.SetStringValue("DICOM_NM_FILE", "");
        if (!"".equals(labelNM.getText())) {
            stSelectedLabel.setText("");
            theSourceStList.clear();
        }
    }//GEN-LAST:event_NMg4dcmButtonActionPerformed

    private void NEVTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_NEVTextFocusLost
        // TODO add your handling code here:
        if (Integer.parseInt(NEVText.getText()) <= 0) {
            JOptionPane.showMessageDialog(null,
                    "Error: Please enter number bigger than 0", "Error Message",
                    JOptionPane.ERROR_MESSAGE);
        }
        theNEvents = Integer.parseInt(NEVText.getText());
        theParamMgr.SetNumericValue("NEVENTS", (double) theNEvents);

        if (GmjVerbose.Verb >= 3) {
            System.out.println("NEVENTS" + theNEvents);
        }
    }//GEN-LAST:event_NEVTextFocusLost

    private void runningDirButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runningDirButtonActionPerformed
        // TODO add your handling code here:
        GmjUtil.showDirDialogAndSetPath(runningDirLabel);
        theParamMgr.SetStringValue("RUNNING_DIR", runningDirLabel.getText());
        if (!"".equals(runningDirLabel.getText())) {
            theRunningDir = runningDirLabel.getText();
        }
        if (CheckGamosIsSet("", true)) {
            enableButtons(true);
        } else {
            enableButtons(false);
        }
    }//GEN-LAST:event_runningDirButtonActionPerformed

    private void isotopesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_isotopesButtonActionPerformed
        // TODO add your handling code here:
        GmjODSelectIsotopeDialog isotD = new GmjODSelectIsotopeDialog(this, true, theIsotopesList);
        JTable isotTable = isotD.GetIsotTable();
        TableModel isotModel = isotTable.getModel();

//        File inputFile = new File("radioactiveIsotopes.lis");
        FileReader fileReader = null;
        try {
            fileReader = new FileReader("radioactiveIsotopes.lis"); //reads the file
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GmjOrganDoseFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedReader bufReader = new BufferedReader(fileReader);
        int nColumns = isotModel.getColumnCount();
        int nRows = isotModel.getRowCount();

        String line = null;
        for (int ir = 0; ir < nRows; ++ir) {
            for (int ic = 0; ic < nColumns; ++ic) {
                try {
                    if ((line = bufReader.readLine()) != null) {
                        isotModel.setValueAt(line, ir, ic);
                    } else {
                        break;
                    }
                } catch (IOException ex) {
                    Logger.getLogger(GmjOrganDoseFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        isotD.setVisible(true);

        theIsotopesList = isotD.GetIsotopesListSelected();
        if (theIsotopesList.size() != 0) {
            theParamMgr.SetVStringValue("ISOTOPES", theIsotopesList);

            // System.out.println("theIsotopesList "+ theIsotopesList.size()); //GDEB
            // theIsotopesList.forEach((temp) -> {
            //     //   String temp = stList.get(ii);
            //     System.out.println(temp); //GDEB
            //  });
            String isotlist = "";
            for (String isot : theIsotopesList) {
                isotlist += isot + " ";
            }
            isotopesLabel.setText(isotlist);
            isotopesLabel.setOpaque(true);
            isotopesLabel.setVisible(true);
            isotopesLabel.setEnabled(true);
        }
    }//GEN-LAST:event_isotopesButtonActionPerformed

    private void effectDoseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effectDoseCheckBoxActionPerformed
        // TODO add your handling code here:
        if (effectDoseCheckBox.isSelected()) {
            if (theStList.isEmpty()) {
                JOptionPane.showMessageDialog(new JFrame(),
                        "Error: Please first select at least one structure");
                return;
            }

            bEffectiveDose = true;

            int nRows = theStList.size();
            int maxchlen = 0;
            for (int ir = 0; ir < nRows; ++ir) {
                maxchlen = Math.max(maxchlen, theStList.get(ir).length());
            }
//            nRows = 24;
            //          maxchlen = 2;
            Math.min(90, maxchlen); // max dialogWith=90*10+40(borders)+60(2nd column width)
            int stNameWidth = maxchlen * 10;

            int dialogWidth = Math.max(470, stNameWidth + 100);
            int tableWidth = Math.min(600 - 40, stNameWidth + 80);
//            tableWidth = Math.max(460-40,tableWidth);

            int dialogHeight = Math.min(700, (nRows + 1) * 16 + 170 + 8);
            dialogHeight = Math.max(360, dialogHeight);
            int tableHeight = Math.min(680 - 230, (nRows + 1) * 16 + 8);
            tableHeight = Math.max(38, tableHeight);
            GmjODEffectiveDoseDialog effdD = new GmjODEffectiveDoseDialog(this, true,
                     dialogWidth, dialogHeight, tableWidth, tableHeight);
            effdD.setResizable(false);

            JTable effdTable = effdD.GetTable();
            DefaultTableModel effdModel = (DefaultTableModel) effdTable.getModel();
            effdModel.setRowCount(nRows);

            effdD.SetStList(theStList);

            effdTable.getColumnModel().getColumn(0).setMinWidth(stNameWidth);
//            effdTable.getColumnModel().getColumn(0).setPreferredWidth(100);
            effdTable.getColumnModel().getColumn(0).setMaxWidth(stNameWidth);
            effdTable.getColumnModel().getColumn(1).setMinWidth(60);
//            effdTable.getColumnModel().getColumn(1).setPreferredWidth(60);
            effdTable.getColumnModel().getColumn(1).setMaxWidth(60);
//-            effdD.getContentPane().setSize(new Dimension(200,100));

            // effdTable.setRowHeight(40);
            effdD.setVisible(true);

            theDoseEffFactors = effdD.theDoseEffFactors;
            //  System.out.println("theDoseEffFactors"+theDoseEffFactors); //GDEB

//        theIsotopesList = isotD.GetIsotopesListSelected(); 
        }

    }//GEN-LAST:event_effectDoseCheckBoxActionPerformed

    private void doseDepoCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doseDepoCheckBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_doseDepoCheckBoxActionPerformed

    private void CTImageRButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CTImageRButtonActionPerformed
        // TODO add your handling code here:
        labelCTSource2.setText(theG4dcmCTFile);
        labelNM.setText("");
        stSelectedLabel.setText("");
        theSourceStList.clear();
    }//GEN-LAST:event_CTImageRButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void NEVTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NEVTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NEVTextActionPerformed

    private void outputDosesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outputDosesButtonActionPerformed
        // TODO add your handling code here
        String figDir = "";
        if (!"".equals(theRunningDir)) {
            figDir = theRunningDir + File.separator;
            if (theIsotopesList.size() >= 1) {
                figDir += "out" + theIsotopesList.get(0) + File.separator + "organDose." + theIsotopesList.get(0);
            }
        }
        GmjODOutputOrganInfoFrame frame = new GmjODOutputOrganInfoFrame(this, figDir);
        frame.setVisible(true);


    }//GEN-LAST:event_outputDosesButtonActionPerformed

    private void outputFiguresButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outputFiguresButtonActionPerformed
        // TODO add your handling code here:
        String figDir = "";
        if (!"".equals(theRunningDir)) {
            figDir = theRunningDir + File.separator;
            if (theIsotopesList.size() >= 1) {
                figDir += "out" + theIsotopesList.get(0) + File.separator + "figs." + theIsotopesList.get(0);
            }
        }
        GmjODOutputFigFrame frame = new GmjODOutputFigFrame(figDir);
        frame.setVisible(true);
    }//GEN-LAST:event_outputFiguresButtonActionPerformed

    private void calcInStCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calcInStCheckBoxActionPerformed
        // TODO add your handling code here:
        if (calcInStCheckBox.isSelected()) {
            bCalculateInStruct = true;
            struct2G4volsButton.setEnabled(true);
        } else {
            bCalculateInStruct = false;
            struct2G4volsButton.setEnabled(false);
        }
    }//GEN-LAST:event_calcInStCheckBoxActionPerformed

    private void RTSTRUCTButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RTSTRUCTButtonActionPerformed
        // TODO add your handling code here:

        GmjUtil.showFileDialogAndSetPath(labelRTSTRUCT);
        AllStRbutton.setEnabled(true);
        SelectStRbutton.setEnabled(true);
        theRTSTRUCTFile = labelRTSTRUCT.getText();

        theParamMgr.SetStringValue("DICOM_RTSTRUCT_FILE", labelRTSTRUCT.getText());
        if (!"".equals(theRTSTRUCTFile)) {
            Boolean isOK = readRTSTRUCT(labelRTSTRUCT.getText()); // read file to fill list of structures
            if (isOK) {
                struct2G4volsButton.setEnabled(true);
            } else {
                labelRTSTRUCT.setText("");
            }
        }
        if (!"".equals(theCTDir)) {
            runCTDICOM2G4();
        }
    }//GEN-LAST:event_RTSTRUCTButtonActionPerformed

    private void readG4dcmCT(String fileName) {

        theG4dcmCTFile = "";

        File file = new File(fileName);
        if (!file.exists()) {
            JOptionPane.showMessageDialog(new JFrame(),
                    "!WARNING: GAMOS CT FILE " + fileName + " DOES NOT EXIST");
            return;
        }

        int numLastLineToRead = 5;

        Boolean bStructFound = false;
        String line = "";
        ReversedLinesFileReader reader = null;
        try {
            reader = new ReversedLinesFileReader(file);
        } catch (IOException ex) {
            Logger.getLogger(GmjOrganDoseFrame.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        try {
            line = reader.readLine();
        } catch (IOException ex) {
            Logger.getLogger(GmjOrganDoseFrame.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        //      System.out.println("LAST LINE "+line); //GDEB
        // if line does not contain string (struct name) stop
        String[] words = line.split(" ");
        for (String word : words) {
            try {
                Double val = Double.parseDouble(word);
            } catch (NumberFormatException nfe) {
                bStructFound = true;
                break;
            }
        }
        if (GmjVerbose.Verb >= 2) {
            System.out.println("READ g4dcm bStructFound " + bStructFound);
        }

        if (bStructFound) {
            AllStRbutton.setEnabled(true);
            SelectStRbutton.setEnabled(true);
            //--- Read Structure IDs
            List<String> newStList = new ArrayList<>();
            List<String> strVector;
            while (true) {
                strVector = GmjFileIn.SplitInWords(line);
                Boolean bLineIsStruct = true;
                for (int ii = 0; ii < strVector.size(); ii++) {
                    try {
                        Double val = Double.parseDouble(strVector.get(ii));
                        bLineIsStruct = false;
                    } catch (NumberFormatException nfe) {
                        bLineIsStruct = true;
                        break;
                    }
                }
                if (!bLineIsStruct) {
                    break;
                }
                newStList.add(GmjUtil.string(strVector.get(1)));
                try {
                    line = reader.readLine();
                } catch (IOException ex) {
                    Logger.getLogger(GmjOrganDoseFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
//                System.out.println("BEFORE LAST LINE "+line); //GDEB
            }

            CheckNewStList(newStList);//check that this list is the same as the one read from RTSTRUCT if it exists

        } else {
            JOptionPane.showMessageDialog(new JFrame(),
                    "!WARNING: GAMOS CT FILE " + fileName + " DOES NOT CONTAIN STRUCTURES");
            return;
        }

        theG4dcmCTFile = fileName;
        labelCT.setText(fileName);
        //   System.out.println("bStructFromDICOM "+bStructFromDICOM); //GDEB
        labelCTSource2.setText(theG4dcmCTFile);
        CTImageRButton.setSelected(true);
        labelCT.setText(fileName);
        theCTDir = "";
        theParamMgr.SetStringValue("GAMOS_CT_FILE", labelCT.getText());

    }

    private Boolean readRTSTRUCT(String fileName) {

        if (!CheckGamosIsSet(fileName, true)) {
            return false;
        }

        File filet = new File(fileName);
        if (!filet.exists()) {
            JOptionPane.showMessageDialog(new JFrame(),
                    "!WARNING: DICOM RTSTRUCT FILE " + fileName + " DOES NOT EXIST");
            calcInStCheckBox.setEnabled(false);
            bCalculateInStruct = false;
            return false;
        }
        GmjTerminal terminal = new GmjTerminal();
        GmjTerminal.Commands commands;
        commands = new GmjTerminal.Commands();
        String dataDir = theGamosPath + "data";
        File dir = new File(theRunningDir);
        dir.mkdir();

        //File file = new File(theRunningDir + File.separator + "extractStruct.sh");
        //    System.out.println("OPEN RTST "+theRunningDir + File.separator+"extractStruct.sh"); //GDEB
        FileWriter fr = null;
        try {
            fr = new FileWriter(fixPath(theRunningDir + File.separator + "extractStruct.sh"));
            fr.write("#!/bin/bash\n");
            fr.write("cd " + theRunningDir + "\n");
            fr.write("source " + theGamosConfFile + "\n");
            fr.write("dcmdump " + fileName
                    + " | grep ROIName | awk '{print $3}' | sed s/\"\\[\"/\"\"/g | sed s/\"\\]\"//g > rts.lis\n");
            fr.write("if [ ! -s rts.lis ]; then\n");
            fr.write("  dcmdump " + fileName
                    + " | grep ROINum | sort | uniq | awk '{print \"Struct\"$3}' | sed s/\"\\[\"/\"\"/g | sed s/\"\\]\"//g > rts.lis\n");
            fr.write("fi\n");
            fr.write("if [ ! -s rts.lis ]; then\n");
            fr.write("  echo !!!ERRR FILE HAS NO STRUCTURE\n");
            fr.write("fi\n");
        } catch (IOException e) {
            e.printStackTrace();
            calcInStCheckBox.setEnabled(false);
            bCalculateInStruct = false;
            return false;
        } finally {
            //close resources
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        commands.add("cd " + theRunningDir);
        commands.add("source " + theGamosConfFile);
        commands.add("bash extractStruct.sh");
        terminal.execWait(commands, outputTextArea, theExecuteLineDelete);

        try {
            Thread.sleep(1000); // wait until dcmdump fills "rts.lis"
        } catch (InterruptedException ex) {
            Logger.getLogger(GmjOrganDoseFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        GmjFileIn fin = new GmjFileIn(fixPath(theRunningDir + File.separator + "rts.lis"));
        List<String> strVector = new ArrayList<>();
        List<String> newStList = new ArrayList<>();
        int ii = 0;
        for (;; ++ii) {
            strVector = fin.GetWordsInLine();
            if (strVector.size() == 0) {
                break;
            }
            newStList.add(GmjUtil.string(strVector.get(0)));
        }

        if (ii == 0) {
            JOptionPane.showMessageDialog(new JFrame(),
                    "!!! ERROR DICOM RTSTRUCT FILE " + fileName
                    + " DOES NOT CONTAIN ANY STRUCTURE");
            System.exit(1);
        }
        if (!"".equals(theRTSTRUCTFile)) {
            CheckNewStList(newStList);//check that this list is the same as the one read from RTSTRUCT if it exists
        } else {
            theStList = newStList;
        }

        AllStRbutton.setEnabled(true);
        SelectStRbutton.setEnabled(true);
        //  System.out.println("nVoxReadTotal "+nVoxReadTotal+" nVoxels "+nVoxels+" isOK"+isOK); //GDEB

        calcInStCheckBox.setEnabled(true);
        //   bCalculateInStruct = true;

        return true;
    }

    public static final String dicomCTmdat = "dicomCT.mdat";

    private void runCTDICOM2G4() {
        if (!CheckGamosIsSet(labelCT.getText(), true)) {
            labelCT.setText("");
            theCTDir = "";
            theParamMgr.SetStringValue("DICOM_CT_DIR", "");
            return;
        }

        //----- READ FILE FROM CT DIRECTORY
        if (GmjVerbose.Verb >= 1) {
            System.out.println("START runCTDICOM2G4");
        }

        //----- ADD CT FILES
        try {
            OutputStreamWriter writer = new OutputStreamWriter(
                    new FileOutputStream(fixPath(theRunningDir + File.separator + dicomCTmdat)), "UTF-8");
            if (GmjVerbose.Verb >= 3) {
                System.out.println("DUMPING :FILE IN " + fixPath(theRunningDir + File.separator + dicomCTmdat) + "\n");
            }
            BufferedWriter bufWriter = new BufferedWriter(writer);
            bufWriter.write(":COMPRESSION 1\n");
            bufWriter.write(":MATE G4_AIR 0.02\n");
            bufWriter.write(":MATE G4_LUNG_ICRP 0.9\n");
            bufWriter.write(":MATE G4_ADIPOSE_TISSUE_ICRP 0.95\n");
            bufWriter.write(":MATE G4_WATER 1.01\n");
            bufWriter.write(":MATE G4_MUSCLE_STRIATED_ICRU 1.04\n");
            bufWriter.write(":MATE G4_B-100_BONE 1.45\n");
            bufWriter.write(":MATE G4_BONE_CORTICAL_ICRP 1.92\n");
            bufWriter.write(":MATE G4_Fe 7.8\n");
            bufWriter.write(":CT2D   -1024   0\n");
            bufWriter.write(":CT2D   -1000   0\n");
            bufWriter.write(":CT2D   -719    0.287\n");
            bufWriter.write(":CT2D   -45     0.975\n");
            bufWriter.write(":CT2D   1       1.022\n");
            bufWriter.write(":CT2D   12      1.028\n");
            bufWriter.write(":CT2D   30      1.039\n");
            bufWriter.write(":CT2D   45      1.047\n");
            bufWriter.write(":CT2D   104     1.081\n");
            bufWriter.write(":CT2D   295     1.192\n");
            bufWriter.write(":CT2D   1558    1.921\n");
            bufWriter.write(":CT2D   10000   1.921+(10000-1558)*(1.921-1.192)/(1558-295)\n");
            for (int ii = 0; ii < theCTFiles.size(); ii++) {
                bufWriter.write(":FILE " + theCTFiles.get(ii) + "\n");
                if (GmjVerbose.Verb >= 3) {
                    System.out.println("DUMPING ADDING :FILE " + theCTFiles.get(ii) + "\n");
                }
            }
            //----- ADD RTSTRUCT FILE
            if (!"".equals(theRTSTRUCTFile)) {
                bufWriter.write(":FILE " + theRTSTRUCTFile + "\n");
            }
            bufWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //----- CREATE getOrganDose DIRECTORY & COPY dicom.mdat.example TO CURRENT DIRECTORY
        GmjTerminal terminal = new GmjTerminal();
        GmjTerminal.Commands commands;
        commands = new GmjTerminal.Commands();
        commands.add("mkdir -p " + theRunningDir);
        commands.add("cd " + theRunningDir);
        //----- RUN DICOM2G4 
        theG4dcmCTFile = fixPath(theRunningDir + File.separator + "testCT.g4dcm");
        commands.add("source " + theGamosConfFile);
        commands.add("DICOM2G4 -f " + dicomCTmdat + " -fOut " + theG4dcmCTFile);
        commands.add("echo !! CT g4dcm FILE WRITTEN " + theG4dcmCTFile);
        //JOptionPane.showMessageDialog(new JFrame(),
        //          "!WARNING: GAMOS_PATH IS NOT VALID ");
        /*    JOptionPane jop = new JOptionPane("Converting DICOM CT FILES TO GAMOS CT FILE \n Please wait..."
                , JOptionPane.INFORMATION_MESSAGE); */
        JDialog dlg = new JDialog(new JFrame(), "");
        dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        // NO block execution
        dlg.setModal(false);  // or dlg.setModalityType(Dialog.ModalityType.MODELESS);
        dlg.setSize(new Dimension(370, 80));
        dlg.setLayout(new FlowLayout());
        dlg.setUndecorated(true);
        dlg.getRootPane().setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, Color.black));
        dlg.add(new JLabel("<html><font size=+0><p><center>Converting DICOM CT FILES TO GAMOS CT FILE</center>"
                + "<center>Please wait...</center></font></html>"));
        dlg.setLocationRelativeTo(this);
        dlg.setVisible(true);
        terminal.execWait(commands, outputTextArea, theExecuteLineDelete);
        dlg.setVisible(false);

        labelCT.setText(theG4dcmCTFile);
        labelCTSource2.setText(theG4dcmCTFile);

        theParamMgr.SetStringValue("GAMOS_CT_FILE", theG4dcmCTFile);

        return;
    }
    public static final String dicomNMmdat = "dicomNM.mdat";

    private void runNMDICOM2G4() throws UnsupportedEncodingException {

        if (!CheckGamosIsSet(labelNM.getText(), true)) {
            labelNM.setText("");
            theNMFile = "";
            theParamMgr.SetStringValue("DICOM_NM_FILE", "");
            return;
        }

        try {
            OutputStreamWriter writer = new OutputStreamWriter(
                    new FileOutputStream(fixPath(theRunningDir + File.separator + dicomNMmdat)), "UTF-8");
            BufferedWriter bufWriter = new BufferedWriter(writer);
            bufWriter.write(":FILE " + theNMFile + "\n");
            bufWriter.close();
            if (GmjVerbose.Verb >= 3) {
                System.out.println("FILE " + dicomNMmdat + " " + fixPath(theRunningDir + File.separator + dicomNMmdat));
            }
        } catch (IOException ex) {
            Logger.getLogger(GmjOrganDoseFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        //----- RUN DICOM2G4 
        theG4dcmNMFile = fixPath(theRunningDir + File.separator + "testNM.g4dcm");
        //----- CREATE getOrganDose DIRECTORY 
        GmjTerminal terminal = new GmjTerminal();
        GmjTerminal.Commands commands;
        commands = new GmjTerminal.Commands();
        commands.add("mkdir -p " + theRunningDir);
        commands.add("cd " + theRunningDir);
        commands.add("source " + theGamosConfFile);
        commands.add("DICOM2G4 -f " + dicomNMmdat + " -fOut " + theG4dcmNMFile);
        commands.add("echo !! NM g4dcm FILE WRITTEN " + theG4dcmNMFile);
        commands.add("ls -l " + dicomNMmdat + " " + theG4dcmNMFile); //GDEB
        JDialog dlg = new JDialog(new JFrame(), "");
        dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        // NO block execution
        dlg.setModal(false);  // or dlg.setModalityType(Dialog.ModalityType.MODELESS);
        dlg.setSize(new Dimension(370, 80));
        dlg.setLayout(new FlowLayout());
        dlg.setUndecorated(true);
        dlg.getRootPane().setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, Color.black));
        dlg.add(new JLabel("<html><font size=+0><p><center>Converting DICOM NM FILES TO GAMOS NM FILE</center>"
                + "<center>Please wait...</center></font></html>"));
        dlg.setLocationRelativeTo(this);
        dlg.setVisible(true);
        terminal.execWait(commands, outputTextArea, theExecuteLineDelete);
        dlg.setVisible(false);

        labelNM.setText(theG4dcmNMFile);

        theParamMgr.SetStringValue("GAMOS_NM_FILE", theG4dcmNMFile);

        return;
    }

    private String buildGAMOSInput(String isotope) {

        String fileName = fixPath(theRunningDir + File.separator + "getOrganDose." + isotope + ".in");
        try {
            OutputStreamWriter writer = new OutputStreamWriter(
                    new FileOutputStream(fileName), "UTF-8");
            BufferedWriter bufWriter = new BufferedWriter(writer);
            //    FileWriter writer = new FileWriter("MyFile.txt", true);
            //          BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufWriter.write("/gamos/setParam GmAnalysisMgr:FileNameSuffix od" + isotope + "\n");
//            bufferedWriter.newLine();
            bufWriter.write("/gamos/setParam GmReadPhantomGeometry:Phantom:SkipEqualMaterials 0\n");
            bufWriter.write("/gamos/setParam GmReadPhantomGeometry:MotherName world\n");
            Boolean bParalVol = true;
            if (bCalculateInStruct) {
                theRTSTRUCTgeomFile = "parallelVolumes.geom";
                File fin = new File(theRTSTRUCTgeomFile);
                if (!fin.exists()) {
                    JOptionPane.showMessageDialog(new JFrame(),
                            "!WARNING: FILE 'parallelVolumes.geom' HAS NOT BEEN CREATED \n"
                            + "press button 'RTSTRUCT polygones to Geant4 volumes");
                    bParalVol = false;
                } else {
                    bufWriter.write("/gamos/setParam GmReadPhantomGeometry:FileNameParallel " + theRTSTRUCTgeomFile + " 1\n");
                }
            }

            //--- GET MAXIMUM WORLD 
            Double wXMax = Double.MAX_VALUE;
            Double wYMax = Double.MAX_VALUE;
            Double wZMax = Double.MAX_VALUE;
            //-- FROM testCT.g4dcm
            GmjFileIn finCT = new GmjFileIn(theG4dcmCTFile);
            List<String> strVector = new ArrayList<>();
            strVector = finCT.GetWordsInLine();
            int nMate = Integer.parseInt(strVector.get(0));
            for (int im = 0; im < nMate; ++im) {
                strVector = finCT.GetWordsInLine();
                if (strVector.size() == 0) {
                    break;
                }
            }
            strVector = finCT.GetWordsInLine();
            strVector = finCT.GetWordsInLine();
            wXMax = Math.max(Math.abs(Double.parseDouble(strVector.get(0))), Math.abs(Double.parseDouble(strVector.get(1))));
            strVector = finCT.GetWordsInLine();
            wYMax = Math.max(Math.abs(Double.parseDouble(strVector.get(0))), Math.abs(Double.parseDouble(strVector.get(1))));
            strVector = finCT.GetWordsInLine();
            wZMax = Math.max(Math.abs(Double.parseDouble(strVector.get(0))), Math.abs(Double.parseDouble(strVector.get(1))));
            if (!"".equals(labelNM.getText())) {
                //-- FROM testNM.g4dcm
                GmjFileIn finNM = new GmjFileIn(theG4dcmNMFile);
                strVector = finCT.GetWordsInLine();
                strVector = finCT.GetWordsInLine();
                wXMax = Math.max(wXMax, Math.abs(Double.parseDouble(strVector.get(0))));
                wXMax = Math.max(wXMax, Math.abs(Double.parseDouble(strVector.get(1))));
                strVector = finCT.GetWordsInLine();
                wYMax = Math.max(wYMax, Math.abs(Double.parseDouble(strVector.get(0))));
                wYMax = Math.max(wYMax, Math.abs(Double.parseDouble(strVector.get(1))));
                strVector = finCT.GetWordsInLine();
                wZMax = Math.max(wZMax, Math.abs(Double.parseDouble(strVector.get(0))));
                wZMax = Math.max(wZMax, Math.abs(Double.parseDouble(strVector.get(1))));
            }
            OutputStreamWriter writerG = new OutputStreamWriter(
                    new FileOutputStream(fixPath(theRunningDir + File.separator + "world.geom")), "UTF-8");
            BufferedWriter bufWriterG = new BufferedWriter(writerG);
            bufWriterG.write(":VOLU world BOX " + wXMax + "+1 " + wYMax + "+1 " + wZMax + "+1 G4_AIR\n");
            bufWriterG.close();

            bufWriter.write("/gamos/setParam GmReadPhantomGeometry:FileName world.geom\n");

            bufWriter.write("/gamos/setParam GmReadPhantomGeometry:Phantom:FileName testCT.g4dcm\n");
            bufWriter.write("/gamos/geometry GmReadPhantomG4withStGeometry\n");
            bufWriter.write("/gamos/physicsList GmEMExtendedPhysics\n");
            bufWriter.write("/gamos/generator GmGenerator\n");
            bufWriter.write("/run/setCut 0.01\n");
            bufWriter.write("/run/initialize\n");
            if (bCalculateInStruct && bParalVol) {
                bufWriter.write("/gamos/physics/addParallelProcess\n");
            }
            String stlist = "";
            if (theSourceStList.size() != 0) {
                stlist = theSourceStList.stream().map((st) -> st + " ").reduce(stlist, String::concat);
                bufWriter.write("/gamos/setParam GmGenerDistPositionInPhantomVoxels " + stlist);
            }
            bufWriter.write("\n");

            bufWriter.write("/gamos/generator/addSingleParticleSource source " + isotope + " 0.\n");

            if (labelNM.getText() != "") {
                bufWriter.write("/gamos/generator/positionDist source GmGenerDistPositionNMImage " + theG4dcmNMFile + "\n");
            } else {
                if ("".equals(stSelectedLabel.getText())) {
                    bufWriter.write("/gamos/generator/positionDist source GmGenerDistPositionInPhantomVoxels \n");
                } else {
                    String sts = "";
                    for (String st : theSourceStList) {
                        sts += st + " ";
                    }
                    bufWriter.write("/gamos/generator/positionDist source GmGenerDistPositionInStPhantomVoxels " + sts + "\n");
                }
            }

            /*            if( ! "".equals(stSelectedLabel.getText()) ) {
                String sts = "";   
                for( String st : theSourceStList ) sts += st+" ";
                bufWriter.write("/gamos/filter startStructVoxelsF GmStartPhantomStructureFilter "+sts+"\n");
                bufWriter.write("/gamos/filter noStartStructVoxelsF GmInverseFilter startStructVoxelsF \n");
                bufWriter.write("/gamos/userAction GmKillAtStackingActionUA GmPrimaryFilter noStartStructVoxelsF\n");
            } */
            bufWriter.write("\n");
            bufWriter.write("/gamos/scoring/createMFDetector doseDet phantom\n");

            if (bCalculateInStruct && bParalVol) {
                bufWriter.write("/gamos/scoring/addScorer2MFD myScorer GmG4PSEnergyDeposit doseDet\n");
                String structList = "";
                for (String st : theSourceStList) {
                    structList += "*" + st + "* ";
                }
                bufWriter.write("/gamos/filter extrudedF GmInParallelLogicalVolumeFilter " + structList + "\n");
                bufWriter.write("/gamos/scoring/addFilter2Scorer extrudedF myScorer\n");
            } else {
                bufWriter.write("/gamos/scoring/addScorer2MFD myScorer GmG4PSDoseDeposit doseDet\n");
            }

            bufWriter.write("/gamos/setParam GmPSPrinterSqdose_myScorer:FileName sqdose.out\n");

            bufWriter.write("/gamos/scoring/addPrinter2Scorer GmPSPrinterSqdose myScorer\n");
            bufWriter.write("\n");

            bufWriter.write("/gamos/userAction GmCountTracksUA\n");
            bufWriter.write("/gamos/random/restoreSeeds 1111 1111\n");
            bufWriter.write("/run/beamOn " + theNEvents + "\n");

            bufWriter.close();
            executeLabel.setText(theRunningDir + "getOrganDose.in");
            executeLabel.setEnabled(true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileName;
    }

    private Boolean CheckGamosIsSet(String fileName, Boolean bMsg) {

        if ("".equals(theGamosPath) || "".equals(theRunningDir)) {
            return false;
        }

        File dir = new File(theGamosPath);
        String fnMsg = "";
        if (!"".equals(fileName)) {
            fnMsg = "\n" + fileName + " CANNOT BE USED";
        }
        if (!dir.isDirectory()) {
            if (bMsg) {
                JOptionPane.showMessageDialog(new JFrame(),
                        "!WARNING: GAMOS_PATH IS NOT VALID " + theGamosPath + fnMsg);
            }
            return false;
        }
        File file1 = new File(theGamosConfFile);
        if (!file1.exists()) {
            if (bMsg) {
                JOptionPane.showMessageDialog(new JFrame(),
                        "!WARNING: GAMOS CONFIGURATION FILE DOES NOT EXIST " + theGamosConfFile + fnMsg);
            }
            return false;
        }

        File dir2 = new File(theRunningDir);
        if (!dir2.isDirectory()) {
            if (bMsg) {
                JOptionPane.showMessageDialog(new JFrame(),
                        "!WARNING: RUNNING_DIR IS NOT VALID " + theRunningDir + fnMsg);
            }
            return false;
        }

        return true;
    }

    private void cleanNMLabel() {
        labelNM.setText("");
        theParamMgr.SetStringValue("GAMOS_NM_FILE", "");
        theParamMgr.SetStringValue("DICOM_NM_FILE", "");
    }

    private void CheckNewStList(List<String> newStList) {
        if (theStList.size() != 0) {
            List<String> sortedList = new ArrayList<>(theStList);
            Collections.sort(sortedList);
            Collections.sort(newStList);
            if (!newStList.equals(sortedList)) {
                String msg = "!!! New Structure list is not the same as previous one !!!\n";
                int sizeM = Math.max(theStList.size(), newStList.size());
                for (int ii = 0; ii < sizeM; ii++) {
                    String str1 = "   ";
                    String str2 = "   ";
                    if (ii < newStList.size()) {
                        str1 = newStList.get(ii);
                    }
                    if (ii < theStList.size()) {
                        str2 = sortedList.get(ii);
                    }
                    msg += String.format("|%s|%s|\n", str1, str2);
                }
                JOptionPane.showMessageDialog(new JFrame(), msg);
                // System.exit(1);
            }
        }
        theStList = newStList;

    }

    public List<String> GetStList() {
        return theStList;
    }

    public void closeWindow() {
        // does not work in GmjOrganDoseFrame.java ??
        theParamMgr.SetNumericValue("WIDTH", Double.valueOf(getWidth()));
        theParamMgr.SetNumericValue("HEIGHT", Double.valueOf(getHeight()));
        theParamMgr.SaveValues();
    }

    private void enableButtons(Boolean val) {
        CTButton.setEnabled(val);
        CTg4dcmButton.setEnabled(val);
        NMButton.setEnabled(val);
        NMg4dcmButton.setEnabled(val);
        RTSTRUCTButton.setEnabled(val);
        NMImageRbutton.setEnabled(val);
        isotopesButton.setEnabled(val);
        executeButton.setEnabled(val);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton AllStRbutton;
    private javax.swing.JButton CTButton;
    private javax.swing.JRadioButton CTImageRButton;
    private javax.swing.JButton CTg4dcmButton;
    private javax.swing.JTextField NEVText;
    private javax.swing.JButton NMButton;
    private javax.swing.JRadioButton NMImageRbutton;
    private javax.swing.JButton NMg4dcmButton;
    private javax.swing.JButton RTSTRUCTButton;
    private javax.swing.JRadioButton SelectStRbutton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup10;
    private javax.swing.ButtonGroup buttonGroup11;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.ButtonGroup buttonGroup6;
    private javax.swing.ButtonGroup buttonGroup7;
    private javax.swing.ButtonGroup buttonGroup8;
    private javax.swing.ButtonGroup buttonGroup9;
    private javax.swing.JCheckBox calcInStCheckBox;
    private javax.swing.JCheckBox doseDepoCheckBox;
    private javax.swing.JPanel dosePanel;
    private javax.swing.JCheckBox effectDoseCheckBox;
    private javax.swing.JButton executeButton;
    private javax.swing.JLabel executeLabel;
    private javax.swing.JLabel gamosPathLabel;
    private javax.swing.JButton gamosbaseButton;
    private javax.swing.JButton isotopesButton;
    private javax.swing.JLabel isotopesLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelCT;
    private javax.swing.JLabel labelCTSource;
    private javax.swing.JLabel labelCTSource2;
    private javax.swing.JLabel labelNEV;
    private javax.swing.JLabel labelNM;
    private javax.swing.JLabel labelPatient;
    private javax.swing.JLabel labelPatient1;
    private javax.swing.JLabel labelPatient2;
    private javax.swing.JLabel labelRTSTRUCT;
    private javax.swing.JLabel labelSource;
    private javax.swing.JButton outputDosesButton;
    private javax.swing.JButton outputFiguresButton;
    private javax.swing.JTextArea outputTextArea;
    private javax.swing.JButton runningDirButton;
    private javax.swing.JLabel runningDirLabel;
    private javax.swing.JLabel stSelectedLabel;
    private javax.swing.JButton struct2G4volsButton;
    private javax.swing.JButton structGeomButton;
    // End of variables declaration//GEN-END:variables

    //--- MY VARIABLES DECLARATION
    private GmjResizableImagePanel thePanel;
    private GmjParameterMgr theParamMgr;

    private String theGamosPath;
    private String theGamosConfFile;
    private String theRunningDir;

    private String theCTDir;
    private String theG4dcmCTFile;
    private String theNMFile;
    private String theG4dcmNMFile;
    private String theRTSTRUCTFile;
    private String theRTSTRUCTgeomFile; // File with G4Extruded solids 
    private List<String> theCTFiles;

    private List<String> theStList;
    private List<String> theSourceStList;
    private Boolean bSelectStOn;

    private int theNEvents;
    private int theExecuteLineDelete;         //--- NUMBER OF LINES TO BE SHOWN IN EXECUTE PANEL

    private Boolean bCalculateInStruct;

    public List<String> theIsotopesList;

    private Boolean bEffectiveDose;
    TreeMap<String, Double> theDoseEffFactors;
}
