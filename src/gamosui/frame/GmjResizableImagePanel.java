/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui.frame;

import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author gamos
 */

public class GmjResizableImagePanel extends JPanel  {

  private java.awt.Image img;
  public GmjResizableImagePanel(String fileName, int width, int height) {
    
    this(new ImageIcon(fileName).getImage(),width,height);
    theFileName = fileName;
    resizeBackgroundImage(width, height);
  //   System.out.print("new GmjResizableImagePanel " + fileName + " " + width + " "+height); //GDEB
        
  }
  
  public GmjResizableImagePanel(java.awt.Image img, int width, int height) {
    this.img = img;
    Dimension size = new Dimension(width,height);//float(img.getWidth()), int(img.getHeight()));
    setPreferredSize(size);
    setMinimumSize(size);
    setMaximumSize(size);
    setSize(size);
    setLayout(null);
  }
  @Override
  public void paintComponent(Graphics g) {
    g.drawImage(img, 0, 0, null);
  }
  
  public void resizeBackgroundImage(int width, int height) {
        BufferedImage image = null;
        ImageIcon sizeImage;
        File imgFile;
        imgFile = new File(theFileName);
        try {
            image = ImageIO.read(imgFile);        // get the image
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Picture "+theFileName+" upload attempted & failed");
            return;
        }
        
        BufferedImage resizedImg = resize(image, width, height);
        this.repaint();
    /*// Resize the image
    // 1. Create a new Buffered Image and Graphic2D object
      BufferedImage resizedImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = resizedImg.createGraphics();
    // 2. Use the Graphic object to draw a new image to the image in the buffer
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(resizedImg, 0, 0, width,height, null);
        g2.dispose(); 
        sizeImage = new ImageIcon(resizedImg);
        img = sizeImage.getImage();
    */
    }
  
  
  
    private static BufferedImage resize(BufferedImage img, int height, int width) {
        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0,0, width, height, 0, 0, width, height, null);
        g2d.dispose();
        return resized;
    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int width, int height, int type) {
        int IMG_WIDTH = width;
        int IMG_CLAHEIGHT = height;
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_CLAHEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_CLAHEIGHT, null);
        g.dispose();
        return resizedImage;
    }
   

  private static BufferedImage resizeImageWithHint(BufferedImage originalImage, int width, int height, int type) {
        int IMG_WIDTH = width;
        int IMG_CLAHEIGHT = height;
    BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_CLAHEIGHT,
        type);
    Graphics2D g = resizedImage.createGraphics();
    g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_CLAHEIGHT, null);
    g.dispose();
    g.setComposite(AlphaComposite.Src);

    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
        RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g.setRenderingHint(RenderingHints.KEY_RENDERING,
        RenderingHints.VALUE_RENDER_QUALITY);
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);

    return resizedImage;
  }
 
  String theFileName;
}

/*
    Image.SCALE_DEFAULT – uses the default image-scaling algorithm.
    Image.SCALE_FAST – uses an image-scaling algorithm that gives higher priority to scaling speed than smoothness of the scaled image.
    Image.SCALE_SMOOTH – uses an image-scaling algorithm that gives higher priority to image smoothness than scaling speed.
    Image.SCALE_REPLICATE – use the image scaling algorithm embodied in the ReplicateScaleFilter class.
    Image.SCALE_AREA_AVERAGING – uses the area averaging image scaling algorithm.
*/