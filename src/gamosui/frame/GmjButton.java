/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamosui.frame;

import gamosui.util.GmjParameterMgr;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import java.awt.Point;
import javax.swing.BorderFactory;

import javax.swing.JLabel;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author Sara
 */
public class GmjButton extends javax.swing.JButton {
    
    
    public GmjButton() {
        super("Gradient Button");
        theType = 1;
        bTwoLinesText = false;
      //  Init();
    }
    
    public void SetType( int typ ){
        theType = typ;
    }
    public GmjButton(int type) {
        super("Gradient Button");
        theType = type;
        
        Init();
    }
    
    private void Init() {
        
        setContentAreaFilled(false);
        setFocusPainted(false); // used for demonstration
        
        bTwoLinesText = false;
    
       // setBorder(new LineBorder(Color.BLUE));
        setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
             /*   Color Cbck = Color.GREEN;;
                if( theType == 2 ) {
                    Cbck = Color.GREEN;
                }
                setBackground(Cbck); */
                iPaint = 1;
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
            /*    Color Cbck = Color.BLUE;
                if( theType == 2 ) {
                    Cbck = Color.BLUE;
                }
                setBackground(Cbck); */
                iPaint = 0;
            }
        });
        
        theParamMgr = GmjParameterMgr.getInstance();
        ReadColors();
    }

    private void ReadColors() {  
        cOutUp1 = theParamMgr.GetColor("BUTCOL_OUTUP1", 210, 210, 210, 255);
        cOutDown1 = theParamMgr.GetColor("BUTCOL_OUTDOWN1", 105, 230, 145, 185);
        cInUp1 = theParamMgr.GetColor("BUTCOL_INUP1", 105, 230, 145, 185);
        cInDown1 = theParamMgr.GetColor("BUTCOL_INDOWN1", 210, 210, 210, 255);
        cOutUp2 = theParamMgr.GetColor("BUTCOL_OUTUP2", 50, 50, 50, 155);
        cOutDown2 = theParamMgr.GetColor("BUTCOL_OUTDOWN2", 0, 0, 0, 255);
        cInUp2 = theParamMgr.GetColor("BUTCOL_INUP2", 0, 0, 0, 255);
        cInDown2 = theParamMgr.GetColor("BUTCOL_INDOWN2", 50, 50, 50, 155);
        cOutUp3 = theParamMgr.GetColor("BUTCOL_OUTUP3", 200, 200, 200, 255);
        cOutDown3 = theParamMgr.GetColor("BUTCOL_OUTDOWN3", 150, 150, 150, 255);
        cInUp3 = theParamMgr.GetColor("BUTCOL_INUP3", 150, 150, 150, 255);
        cInDown3 = theParamMgr.GetColor("BUTCOL_INDOWN3", 200, 200, 200, 255);
        cOutUp4 = theParamMgr.GetColor("BUTCOL_OUTUP4", 255, 255, 255, 255);
        cOutDown4 = theParamMgr.GetColor("BUTCOL_OUTDOWN4", 120, 120, 255, 255);
        cInUp4 = theParamMgr.GetColor("BUTCOL_INUP4", 120, 120, 255, 255);
        cInUp4 = theParamMgr.GetColor("BUTCOL_INDOWN4", 255, 255, 255, 255);
        cOutUp5 = theParamMgr.GetColor("BUTCOL_OUTUP5", 0, 0, 120, 155);
        cOutDown5 = theParamMgr.GetColor("BUTCOL_OUTDOWN5", 0, 0, 80, 255);
        cInUp5 = theParamMgr.GetColor("BUTCOL_INUP5", 0, 0, 80, 255);
        cInDown5 = theParamMgr.GetColor("BUTCOL_INDOWN5", 0, 0, 120, 155);
        cOutUp6 = theParamMgr.GetColor("BUTCOL_OUTUP6", 255, 255, 255, 255);
        cOutDown6 = theParamMgr.GetColor("BUTCOL_OUTDOWN6", 240, 100, 40, 155);
        cInUp6 = theParamMgr.GetColor("BUTCOL_INUP6", 240, 100, 40, 155);
        cInDown6 = theParamMgr.GetColor("BUTCOL_INDOWN6", 255, 255, 255, 255);
    }
            
    public void SetText2Lines(String text1, String text2 ) {
        theText1 = text1;
        theText2 = text2;
        bTwoLinesText = true;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        if( bTwoLinesText ) {
/*            setText("");
            setLayout(new BorderLayout());
            JLabel label1 = new JLabel("Convert RTSTRUCT lines");
            JLabel label2 = new JLabel("to Geant4 volumes");
            add(BorderLayout.NORTH,label1);
            add(BorderLayout.SOUTH,label2);
   */         setText("<html><font size=-1><b><center>Convert RTSTRUCT lines</center></b>"
                                 + "<p><center>tao Geant4 volumes</center></html>");
        }
/* if two lines
        buttonSt2G4vols.setText("<html><font size=-1><b><center>Convert RTSTRUCT lines</center></b>"
                                 + "<p><center>to Geant4 volumes</center></html>");
*/
        Color CoutUp = Color.WHITE;
        Color CoutDown = Color.PINK.darker();
        Color CinUp = Color.RED;
        Color CinDown = Color.BLUE; //.brighter();
        switch (theType) {
            case 1:
                CoutUp = cOutUp1;
                CoutDown = cOutDown1;
                CinUp = cInUp1;
                CinDown = cInDown1;
                break;
            case 2:
                CoutUp = cOutUp2;
                CoutDown = cOutDown2;
                CinUp = cInUp2;
                CinDown = cInDown2;
                break;
            case 3:
                CoutUp = cOutUp3;
                CoutDown = cOutDown3;
                CinUp = cInUp3;
                CinDown = cInDown3;
                break;
            case 4:
                CoutUp = cOutUp4;
                CoutDown = cOutDown4;
                CinUp = cInUp4;
                CinDown = cInDown6;
                break;
            case 5:
                CoutUp = cOutUp5;
                CoutDown = cOutDown5;
                CinUp = cInUp5;
                CinDown = cInDown5;
                break;
            case 6:
                CoutUp = cOutUp6;
                CoutDown = cOutDown6;
                CinUp = cInUp6;
                CinDown = cInDown6;
                break;
            default:
                break;
        }
        final Graphics2D g2 = (Graphics2D) g.create();
        if( iPaint == 0 ) { 
            g2.setPaint(new GradientPaint(
                new Point(0, 0), 
                    CoutUp, 
                    new Point(0, getHeight()), 
                    CoutDown));
        } else if( iPaint == 1 ) { 
            g2.setPaint(new GradientPaint(
                new Point(0, 0), 
                    CinUp, 
                    new Point(0, getHeight()), 
                    CinDown));
        }
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.dispose();

        super.paintComponent(g);
    }

    private int iPaint = 0;
    private int theType;
    private Boolean bTwoLinesText;
    private String theText1;
    private String theText2;
    Color cOutUp1;
    Color cOutDown1;
    Color cInUp1;
    Color cInDown1;
    Color cOutUp2;
    Color cOutDown2;
    Color cInUp2;
    Color cInDown2;
    Color cOutUp3;
    Color cOutDown3;
    Color cInUp3;
    Color cInDown3;
    Color cOutUp4;
    Color cOutDown4;
    Color cInUp4;
    Color cInDown4;
    Color cOutUp5;
    Color cOutDown5;
    Color cInUp5;
    Color cInDown5;
    Color cOutUp6;
    Color cOutDown6;
    Color cInUp6;
    Color cInDown6;
    GmjParameterMgr theParamMgr;
        
}
